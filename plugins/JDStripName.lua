ardour {
  ["type"]    = "dsp",
  name        = "JD Strip Name",
  category    = "utility inline-ui",
  license     = "MIT",
  author      = "John Devlin",
  description = [[(v1.0) Displays the strip name. Intended for the "Show Sends" bus/foldback action in Mixbus, because that hides the strip name.]]
}

local displayedName = ""
local MAX_LEN = 20

function dsp_ioconfig ()
  -- -1, -1 = any number of channels as long as input and output count matches
  return { { audio_in = -1, audio_out = -1} }
end

function dsp_params ()
  return {}
end

function dsp_init (rate)
  self:shmem():allocate(MAX_LEN + 1)
  self:shmem():atomic_set_int(0, 0)
end

function dsp_configure (ins, outs)
  n_audio = outs:n_audio()
end

function dsp_run (ins, outs, n_samples)

  -- direct transfer from ins to outs
  for c = 1, n_audio do
    if ins[c] ~= outs[c] then
      ARDOUR.DSP.copy_vector (outs[c]:offset(0), ins[c]:offset(0), n_samples)
    end
  end

  local routeName = self:route():name()
  if displayedName ~= routeName then
    displayedName = routeName
    local len = math.min(displayedName:len(), MAX_LEN)
    self:shmem():atomic_set_int(0, len)
    for i = 1,len do
      self:shmem():atomic_set_int(i, displayedName:byte(i))
    end
    -- notify display
    self:queue_draw()
  end

end

function render_inline (ctx, w, max_h)

  local h = 13
  local hMargin = 3
  local x

  local len = self:shmem():atomic_get_int(0)
  local displayName = ""
  for i = 1,len do
    displayName = displayName..string.char(self:shmem():atomic_get_int(i))
  end

  local iconFont = "ArdourSans"
  local txt = Cairo.PangoLayout (ctx, iconFont.." bold ".. (h - 2) .. "px")
  txt:set_text(displayName)
  local tw, th = txt:get_pixel_size()
  x = math.max(hMargin + (((w - (hMargin * 2) - tw)) / 2), hMargin)
  ctx:move_to(x, 0)
  ctx:set_source_rgba(1.0, 1.0, 1.0, 0.7)
  txt:show_in_cairo_context(ctx)

  return {w, h + 4}
end
