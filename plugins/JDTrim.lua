ardour {
  ["type"]    = "dsp",
  name        = "JD Trim",
  category    = "utility spatial inline-ui",
  license     = "MIT",
  author      = "John Devlin",
  description = [[Various controls for L and R channels: trim, polarity inversion, mute and channel swap.]],
  version     = 2.0
}

-- return possible i/o configurations
function dsp_ioconfig ()
  return { [1] = { audio_in = 2, audio_out = 2}, }
end

function dsp_params ()
  return {
    { ["type"] = "input", name = "Trim L", min = -20, max = 20, default = 0 },
    { ["type"] = "input", name = "Trim R", min = -20, max = 20, default = 0 },
    { ["type"] = "input", name = "Invert Polarity L", min = 0, max = 1, default = 0, toggled = true },
    { ["type"] = "input", name = "Invert Polarity R", min = 0, max = 1, default = 0, toggled = true },
    { ["type"] = "input", name = "Mute L", min = 0, max = 1, default = 0, toggled = true },
    { ["type"] = "input", name = "Mute R", min = 0, max = 1, default = 0, toggled = true },
    { ["type"] = "input", name = "Swap L/R Outs", min = 0, max = 1, default = 0, toggled = true },
   }
end

local transitionMs = 5 -- gain transition duration in milliseconds
local transitionSamples = 0 -- gain transition duration in samples (calculated later based on transitionMs and sample rate)
local transitionSamplesStillToProcess = 0
local isb = nil -- memory input scratch buffer
local gtb = nil -- memory gain transition buffer
local mb = { nil, nil } -- memory mix buffers

local params = {
  count = 7,  -- number of params
  prevValues = {},
  phaseRevs = { false, false },
  dbTrims = { 0, 0 }, -- the amount to trim in dB: [1] = L, [2] = R
  mutes = { 0, 0 },
  swapped = false,
  anyEngaged = false -- true if either trim ~= 0 or any other options are on
}

-- current and target gains: [1][1] = L in L out, [1][2] = L in R out, [2][1] = R in L out, [2][2] = R in R out. targetGains[n][3] is the target gain independant of the output channel.
local curGains = { { 1, 0 }, { 0, 1 } }
local targetGains = { { 1, 0, 1 }, { 0, 1, 1 } }

function dsp_init (rate)

  transitionSamples = math.floor((rate / 1000) * transitionMs)

  -- allocate buffers
  isb = ARDOUR.DSP.DspShm(8192)
  gtb = ARDOUR.DSP.DspShm(8192)
  mb[1] = ARDOUR.DSP.DspShm(8192)
  mb[2] = ARDOUR.DSP.DspShm(8192)
  isb:clear()
  gtb:clear()
  mb[1]:clear()
  mb[2]:clear()
end

function dsp_configure (ins, outs)
end

-- the DSP callback function
function dsp_run (ins, outs, n_samples)

  assert(n_samples <= 8192) -- ensure the number of samples is not larger than our buffers

  local paramsChanged = getParams()

  if paramsChanged then
    calcTargetGains()
    transitionSamplesStillToProcess = transitionSamples
  end

  if transitionSamplesStillToProcess <= 0 then

    -- no transition required

    if params.anyEngaged then

      for c = 1, 2 do

        ARDOUR.DSP.copy_vector(mb[c]:to_float(0), ins[c]:offset(0), n_samples)

        local g = targetGains[c][3]

        if g ~= 1 then
          ARDOUR.DSP.apply_gain_to_buffer(mb[c]:to_float(0), n_samples, g)
        end

      end

      if params.swapped then
        ARDOUR.DSP.copy_vector (outs[1]:offset(0), mb[2]:to_float(0), n_samples)
        ARDOUR.DSP.copy_vector (outs[2]:offset(0), mb[1]:to_float(0), n_samples)
      else
        ARDOUR.DSP.copy_vector (outs[1]:offset(0), mb[1]:to_float(0), n_samples)
        ARDOUR.DSP.copy_vector (outs[2]:offset(0), mb[2]:to_float(0), n_samples)
      end


    else
      -- none engaged: direct transfer from ins to outs

      for c = 1, 2 do
        if ins[c] ~= outs[c] then
          ARDOUR.DSP.copy_vector (outs[c]:offset(0), ins[c]:offset(0), n_samples)
        end
      end

    end

  else

    -- transition

    for cin = 1, 2 do
      for cout = 1, 2 do

        ARDOUR.DSP.copy_vector(isb:to_float(0), ins[cin]:offset(0), n_samples)
        transitionGainChange(cin, cout, n_samples)
        if cin == 1 then
          ARDOUR.DSP.copy_vector(mb[cout]:to_float(0), isb:to_float(0), n_samples)
        else
          ARDOUR.DSP.mix_buffers_no_gain(mb[cout]:to_float(0), isb:to_float(0), n_samples)
        end

      end
    end

    ARDOUR.DSP.copy_vector(outs[1]:offset(0), mb[1]:to_float(0), n_samples)
    ARDOUR.DSP.copy_vector(outs[2]:offset(0), mb[2]:to_float(0), n_samples)

    -- The transition might not yet be complete so store where we're up to.
    -- It might have processed fewer than n_samples but that's okay - if it did then the transition is
    -- finished anyway. It will result in transitionSamplesStillToProcess being < 0 and that too is okay.
    transitionSamplesStillToProcess = transitionSamplesStillToProcess - n_samples

  end

  if paramsChanged then
    -- notify display
    self:queue_draw()
  end

end

function getParams()

  -- Get the parameters from the controls and store them in params.
  -- Returns true if any parameter does not match its current value in params, i.e. it has changed since this was last called.

  local ctrl = CtrlPorts:array() -- get control port array
  local changed = false

  for c = 1, params.count do
    if ctrl[c] ~= params.prevValues[c] then changed = true end
    params.prevValues[c] = ctrl[c]
  end

  params.dbTrims[1] = ctrl[1]
  params.dbTrims[2] = ctrl[2]
  params.phaseRevs[1] = ctrl[3] > 0
  params.phaseRevs[2] = ctrl[4] > 0
  params.mutes[1] = ctrl[5] > 0
  params.mutes[2] = ctrl[6] > 0
  params.swapped = ctrl[7] > 0

  params.anyEngaged = params.phaseRevs[1] or params.phaseRevs[2] or 
                      params.dbTrims[1] ~= 0 or params.dbTrims[2] ~= 0 or
                      params.mutes[1] ~= 0 or params.mutes[2] ~= 0 or
                      params.swapped

  return changed
end

function calcTargetGains()

  local gL = ARDOUR.DSP.dB_to_coefficient(params.dbTrims[1])
  local gR = ARDOUR.DSP.dB_to_coefficient(params.dbTrims[2])

  if params.phaseRevs[1] then gL = gL * -1 end
  if params.phaseRevs[2] then gR = gR * -1 end

  if params.mutes[1] then gL = 0 end
  if params.mutes[2] then gR = 0 end

  if params.swapped then
    targetGains = { { 0, gL, gL }, { gR, 0, gR } }
  else
    targetGains = { { gL, 0, gL }, { 0, gR, gR } }
  end

end

function transitionGainChange(cin, cout, n_samples)

  -- Apply gain transition to isb buffer. When parameters change, transition from the current gain (gfrom)
  -- to the target gain (gto) over a period of time (transitionMs) for smoother results.

  if n_samples == 0 then return end
  if transitionSamplesStillToProcess == 0 then return end

  local gfrom = curGains[cin][cout]
  local gto = targetGains[cin][cout]

  local steps = math.min(transitionSamplesStillToProcess, n_samples)

  local adjPerStep = (gto - gfrom) / transitionSamplesStillToProcess

  local a = {}

  for i = 1, steps do
    a[i] = gfrom + (adjPerStep * i)
  end

  -- If n_samples < transitionSamplesStillToProcess then we didn't have enough samples this round to
  -- reach the target gain. So the current gain is wherever we got up to.
  local gcur = gto
  if n_samples < transitionSamplesStillToProcess then
    gcur = a[n_samples]
  end

  -- If n_samples > transitionSamplesStillToProcess then a[1]..a[steps] contain the completed transition
  -- and everything after that is at the target gain
  for i = steps + 1, n_samples do
    a[i] = gto
  end

  -- store the multipliers in gtb buffer
  gtb:to_float(0):set_table(a, n_samples)

  -- apply the gtb buffer to the the input scratch buffer, i.e. apply the gain multipliers to isb
  ARDOUR.DSP.mmult(isb:to_float(0), gtb:to_float(0), n_samples)

  --store the current gain
  curGains[cin][cout] = gcur

end

function round(n)
  return math.floor(n + .5)
end

function roundOff(n)
  return -0.5 + math.floor(n + .5)
end

function render_inline (ctx, w, max_h) --inline display

  local ctrl = CtrlPorts:array()
  local trimValL = ctrl[1]
  local trimValR = ctrl[2]
  local phaseL = ctrl[3] > 0
  local phaseR = ctrl[4] > 0
  local muteL = ctrl[5] > 0
  local muteR = ctrl[6] > 0
  local swapped = ctrl[7] > 0
  local gridX = 7
  local gridY = 2
  local gridW = w - (2 * gridX)

  local x, y

  local phase = {}
  phase.X = gridX + 3
  phase.YL = gridY + 3
  phase.YR = phase.YL + 8

  local swap = {}
  swap.W1 = 3 -- segment 1 (ins)
  swap.W2 = 4 -- segment 2 (crossover)
  swap.W3 = 3 -- segment 3 (outs)
  swap.X4 = w - 4
  swap.X3 = swap.X4 - swap.W3
  swap.X2 = swap.X3 - swap.W2
  swap.X1 = swap.X2 - swap.W1
  swap.YL = gridY + 4
  swap.YR = swap.YL + 7

  local trim = {}
  trim.barX = phase.X + 8
  trim.barW = w - (trim.barX * 2)
  trim.barYL = swap.YL
  trim.barYR = swap.YR
  trim.barXC = trim.barX + (trim.barW / 2)
  trim.markerXL = trim.barX + (((trimValL + 20) / 40) * trim.barW)
  trim.markerXR = trim.barX + (((trimValR + 20) / 40) * trim.barW)

  local h = phase.YR + 6


  -- background
  ctx:rectangle(0, 0, w, h)
  ctx:set_source_rgba(0, 0, 0, 1.0)
  ctx:fill()

  -- trims -----------

  -- grid
  ctx:set_line_width(1)
  x = trim.barX
  y = trim.barYL
  if muteL then
    ctx:set_source_rgba(0.3, 0, 0, 1.0)
  else
    ctx:set_source_rgba(0.25, 0.25, 0.25, 1.0)
  end
  ctx:move_to(roundOff(x), roundOff(y))
  ctx:line_to(roundOff(x + trim.barW), roundOff(y))
  ctx:stroke()
  y = trim.barYR
  if muteR then
    ctx:set_source_rgba(0.3, 0, 0, 1.0)
  else
    ctx:set_source_rgba(0.25, 0.25, 0.25, 1.0)
  end
  ctx:move_to(roundOff(x), roundOff(y))
  ctx:line_to(roundOff(x + trim.barW), roundOff(y))
  ctx:stroke()

  -- marker range
  ctx:set_line_width(1)
  ctx:set_source_rgba(0.483, 0.597, 0.766, 1.0)
  if not muteL then
    y = roundOff(trim.barYL)
    ctx:move_to(roundOff(trim.barXC), y)
    ctx:line_to(roundOff(trim.markerXL), y)
    ctx:stroke()
  end
  if not muteR then
    y = roundOff(trim.barYR)
    ctx:move_to(roundOff(trim.barXC), y)
    ctx:line_to(roundOff(trim.markerXR), y)
    ctx:stroke()
  end

  -- marker point
  ctx:set_source_rgba(0.483, 0.597, 0.766, 1.0)
  if not muteL then
    ctx:move_to(roundOff(trim.markerXL), roundOff(trim.barYL - 2))
    ctx:line_to(roundOff(trim.markerXL), roundOff(trim.barYL + 2))
    ctx:stroke()
  end
  if not muteR then
    ctx:move_to(roundOff(trim.markerXR), roundOff(trim.barYR - 2))
    ctx:line_to(roundOff(trim.markerXR), roundOff(trim.barYR + 2))
    ctx:stroke()
  end

  -- phase -----------

  -- background
  if phaseL and not muteL then
    ctx:set_source_rgba(0.180, 0.255, 0.902, 1.0)
    ctx:arc(roundOff(phase.X), roundOff(phase.YL), 4, 0, 2 * math.pi)
    ctx:fill()
  end

  if phaseR and not muteR then
    ctx:set_source_rgba(0.180, 0.255, 0.902, 1.0)
    ctx:arc(roundOff(phase.X), roundOff(phase.YR), 4, 0, 2 * math.pi)
    ctx:fill()
  end

  -- icon
  if phaseL and not muteL then
    ctx:set_line_width(0.5)
    ctx:set_source_rgba(1.0, 1.0, 1.0, 1.0)
    ctx:arc(roundOff(phase.X), roundOff(phase.YL), 3, 0, 2 * math.pi)
    ctx:stroke()
    ctx:move_to(roundOff(phase.X + 4), roundOff(phase.YL - 4))
    ctx:line_to(roundOff(phase.X - 4), roundOff(phase.YL + 4))
    ctx:stroke()
  end
  if phaseR and not muteR then
    ctx:set_line_width(0.5)
    ctx:set_source_rgba(1.0, 1.0, 1.0, 1.0)
    ctx:arc(roundOff(phase.X), roundOff(phase.YR), 3, 0, 2 * math.pi)
    ctx:stroke()
    ctx:move_to(roundOff(phase.X + 4), roundOff(phase.YR - 4))
    ctx:line_to(roundOff(phase.X - 4), roundOff(phase.YR + 4))
    ctx:stroke()
  end

  -- swap -----------

  if swapped then
    ctx:set_line_width(1.5)
    ctx:set_line_join(Cairo.LineJoin.Round)
    ctx:set_source_rgba(0.541, 0.513, 0.055, 1.0)
    ctx:move_to(roundOff(swap.X1), roundOff(swap.YR))
    ctx:line_to(roundOff(swap.X2), roundOff(swap.YR))
    ctx:line_to(roundOff(swap.X3), roundOff(swap.YL))
    ctx:line_to(roundOff(swap.X4), roundOff(swap.YL))
    ctx:stroke()
    ctx:set_source_rgba(0.377, 0.582, 0.184, 1.0)
    ctx:move_to(roundOff(swap.X1), roundOff(swap.YL))
    ctx:line_to(roundOff(swap.X2), roundOff(swap.YL))
    ctx:line_to(roundOff(swap.X3), roundOff(swap.YR))
    ctx:line_to(roundOff(swap.X4), roundOff(swap.YR))
    ctx:stroke()
  end

  return {w, h}
end
