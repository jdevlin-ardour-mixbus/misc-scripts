ardour {
  ["type"]    = "dsp",
  name        = "JD Pan+",
  category    = "spatial inline-ui",
  license     = "MIT",
  author      = "John Devlin",
  description = [[Stereo Pan (not Stereo Balance) with minimalist controls and inline display. This "+" variant adds pan law options.]],
  version     = 2.1
}

-- return possible i/o configurations
function dsp_ioconfig ()
  return { [1] = { audio_in = 2, audio_out = 2}, }
end

local PANLAW_ARDOUR = 0
local PANLAW_LINEAR = 1
-- local PANLAW_SINE = 2
-- local PANLAW_SQRT = 3

local DEFAULT_PANLAW = PANLAW_ARDOUR
local DEFAULT_WIDTH = 100
local DEFAULT_PAN = 0

--Pan range is -90..90 in the UI for consistency with Mixbus. Internally it is translated to -100..100 to simplify calculations.

function dsp_params ()
  return {
    { ["type"] = "input", name = "Width", min = 0, max = 100, default = DEFAULT_WIDTH },
    { ["type"] = "input", name = "Pan", min = -90, max = 90, default = DEFAULT_PAN },
    { ["type"] = "input", name = "Pan Law", min = PANLAW_ARDOUR, max = PANLAW_LINEAR, default = DEFAULT_PANLAW, enum = true, scalepoints =
			{
				["-3dB (Ardour/Mixbus)"] = PANLAW_ARDOUR,
				["-6dB (Linear Amplitude)"] = PANLAW_LINEAR,
			}
		},
  }
end

local ARDOUR_SCALE = -0.831783138

local transitionMs = 5.0  -- gain transition duration in milliseconds, regardless of sample rate
local transitionSamples = 0  -- gain transition duration in samples (calculated later based on transitionMs and sample rate)
local transitionSamplesStillToProcess = 0
local isb = nil -- memory input scratch buffer
local gtb = nil -- memory gain transition buffer
local mb = { nil, nil } -- memory mix buffers

local params = {
  count = 3,  -- number of params
  prevValues = {},
  width = DEFAULT_WIDTH,
  pan = DEFAULT_PAN,
  panLaw = DEFAULT_PANLAW
}

-- current and target gains: [1][1] = L in L out, [1][2] = L in R out, [2][1] = R in L out, [2][2] = R in R out
local curGains = { { 1, 0 }, { 0, 1 }}
local targetGains = { { 1, 0 }, { 0, 1 }}

function dsp_init (rate)

  transitionSamples = math.floor((rate / 1000) * transitionMs)

  -- allocate buffers
  isb = ARDOUR.DSP.DspShm(8192)
  gtb = ARDOUR.DSP.DspShm(8192)
  mb[1] = ARDOUR.DSP.DspShm(8192)
  mb[2] = ARDOUR.DSP.DspShm(8192)
  isb:clear()
  gtb:clear()
  mb[1]:clear()
  mb[2]:clear()
end

function dsp_configure (ins, outs)
end

-- the DSP callback function
function dsp_run (ins, outs, n_samples)

  assert(n_samples <= 8192) -- ensure the number of samples is not larger than our buffers

  local paramsChanged = getParams()

  if params.width == 100 and curGains[1][1] == 1 and curGains[2][2] == 1 then
    -- optimisation: requested full width which we're already at (no transition), so just send L and R directly out

    for c = 1, 2 do
      if ins[c] ~= outs[c] then
        ARDOUR.DSP.copy_vector (outs[c]:offset(0), ins[c]:offset(0), n_samples)
      end
    end

  else

    if paramsChanged then
      calcTargetGains()
      transitionSamplesStillToProcess = transitionSamples
    end

    for cin = 1, 2 do
      for cout = 1, 2 do

        ARDOUR.DSP.copy_vector(isb:to_float(0), ins[cin]:offset(0), n_samples)
        if transitionSamplesStillToProcess > 0 then
          transitionGainChange(cin, cout, n_samples)
        elseif targetGains[cin][cout] > 0 then
          ARDOUR.DSP.apply_gain_to_buffer(isb:to_float(0), n_samples, targetGains[cin][cout])
        else
          ARDOUR.DSP.memset(isb:to_float(0), 0, n_samples)
        end
        if cin == 1 then
          ARDOUR.DSP.copy_vector(mb[cout]:to_float(0), isb:to_float(0), n_samples)
        else
          ARDOUR.DSP.mix_buffers_no_gain(mb[cout]:to_float(0), isb:to_float(0), n_samples)
        end

      end
    end

    ARDOUR.DSP.copy_vector(outs[1]:offset(0), mb[1]:to_float(0), n_samples)
    ARDOUR.DSP.copy_vector(outs[2]:offset(0), mb[2]:to_float(0), n_samples)

    -- The transition might not yet be complete so store where we're up to.
    -- It might have processed fewer than n_samples but that's okay - if it did then the transition is
    -- finished anyway. It will result in transitionSamplesStillToProcess being < 0 and that too is okay.
    transitionSamplesStillToProcess = transitionSamplesStillToProcess - n_samples

  end

  if paramsChanged then
    -- notify display
    self:queue_draw()
  end

end

function getParams()

  -- Get the parameters from the controls and store them in params.
  -- Returns true if any parameter does not match its current value in params, i.e. it has changed since this was last called.

  local ctrl = CtrlPorts:array() -- get control port array
  local changed = false

  for c = 1, params.count do
    if ctrl[c] ~= params.prevValues[c] then changed = true end
    params.prevValues[c] = ctrl[c]
  end

  params.width = ctrl[1]
  params.pan = ctrl[2] * 100 / 90  -- convert -90..90 to -100..100
  params.panLaw = ctrl[3]

  return changed
end

function calcPanGains(pan)

  local x = (pan / 200) + 0.5
  local gl, gr

  if params.panLaw == PANLAW_ARDOUR then
    --This is based on Ardour source code: panner_2in2out.cc. It seems to be some kind of constant power
    --algorithm because it gives very similar results to PANLAW_SINE which is.
    gl = 1 - x
    gr = x
    gl = gl * (ARDOUR_SCALE * gl + 1 - ARDOUR_SCALE)
    gr = gr * (ARDOUR_SCALE * gr + 1 - ARDOUR_SCALE)
  else  -- linear
    gl = 1 - x
    gr = x
  end

  -- The Sine and Sqrt laws are leftovers from initial experiments. They do not appear as options in the UI.

  -- elseif panLaw == PANLAW_SINE then
  --   gl = math.sin((1 - x) * (math.pi / 2))
  --   gr = math.sin(x * (math.pi / 2))
  -- elseif panLaw == PANLAW_SQRT then
  --   x = math.max(x, 0) -- avoid sqrt(-ve)
  --   x = math.min(x, 1) -- avoid sqrt(-ve)
  --   gl = math.sqrt(1 - x)
  --   gr = math.sqrt(x)

  return gl, gr
end

function calcTargetGains()

  -- Calculate actual pan amount. This is the requested pan amount restricted by the current width.
  -- E.g. If width == 75% then the most we can pan is 25%: range -25..+25.
  local actualPan = math.min(100 - params.width, math.abs(params.pan))
  if params.pan < 0 then
    actualPan = actualPan * -1
  end

  for cin = 1, 2 do

    -- width is range 0..100: convert this to p (pan) of range -100..100.
    -- w --> p, for L becomes: 100 --> -100 (pan full left), 0 --> 0 (pan center), 50 --> -50 (pan half left)
    -- w --> p, for R becomes: 100 --> 100 (pan full right), 0 --> 0 (pan center), 50 --> 50 (pan half right)
    local p = params.width
    if cin == 1 then
      p = (params.width * -1)
    end

    -- add the amount it has been panned by
    p = p + actualPan

    local gl, gr = calcPanGains(p)

    targetGains[cin][1] = gl
    targetGains[cin][2] = gr
  end

end

function transitionGainChange(cin, cout, n_samples)

  -- Apply gain transition to isb buffer. When parameters change, transition from the current gain (gfrom)
  -- to the target gain (gto) over a period of time (transitionMs) for smoother results.

  if n_samples == 0 then return end
  if transitionSamplesStillToProcess == 0 then return end

  local gfrom = curGains[cin][cout]
  local gto = targetGains[cin][cout]

  local steps = math.min(transitionSamplesStillToProcess, n_samples)

  local adjPerStep = (gto - gfrom) / transitionSamplesStillToProcess

  local a = {}

  for i = 1, steps do
    a[i] = gfrom + (adjPerStep * i)
  end

  -- If n_samples < transitionSamplesStillToProcess then we didn't have enough samples this round to
  -- reach the target gain. So the current gain is wherever we got up to.
  local gcur = gto
  if n_samples < transitionSamplesStillToProcess then
    gcur = a[n_samples]
  end

  -- If n_samples > transitionSamplesStillToProcess then a[1]..a[steps] contain the completed transition
  -- and everything after that is at the target gain
  for i = steps + 1, n_samples do
    a[i] = gto
  end

  -- store the multipliers in gtb buffer
  gtb:to_float(0):set_table(a, n_samples)

  -- apply the gtb buffer to the the input scratch buffer, i.e. apply the gain multipliers to isb
  ARDOUR.DSP.mmult(isb:to_float(0), gtb:to_float(0), n_samples)

  --store the current gain
  curGains[cin][cout] = gcur

end



function round(n)
  return math.floor(n + .5)
end

function render_inline (ctx, w, max_h) --inline display

  local ctrl = CtrlPorts:array()
  local panWidth = ctrl[1]
  local requestedPanPos = ctrl[2] * 100 / 90  -- convert -90..90 to -100..100
  local gridx = 7
  local gridy = 2
  local gridw = w - (2 * gridx)

  local actualPanPos = math.min(100 - panWidth, math.abs(requestedPanPos))
  if requestedPanPos < 0 then
    actualPanPos = actualPanPos * -1
  end
  local leftPanPos = (panWidth * -1) + actualPanPos

  local panx = gridx + ((((actualPanPos + 100) / 2) / 100) * gridw)

  local barx = gridx + ((((leftPanPos + 100) / 2) / 100) * gridw)
  local bary = gridy + 7
  local barw = gridw * (panWidth / 100)
  local barh = 5

  local h = bary + barh + 6

  -- background
  ctx:rectangle(0, 0, w, h)
  ctx:set_source_rgba(0, 0, 0, 1.0)
  ctx:fill()

  -- markers, major (-100, -50, 0, +50, +100)
  ctx:set_line_width(1)
  ctx:set_source_rgba(0.5, 0.5, 0.5, 1.0)
  for i = 0, 4 do
    local x = -.5 + round(gridx + (i * 0.25 * gridw))
    ctx:move_to (x, gridy)
    ctx:line_to (x, gridy + 3)
    ctx:stroke()
  end

  -- markers, minor
  ctx:set_source_rgba(0.5, 0.5, 0.5, 0.8)
  for i = 0, 20 do
    local x = -.5 + round(gridx + (i * 0.05 * gridw))
    ctx:move_to (x, gridy)
    ctx:line_to (x, gridy + 2)
    ctx:stroke()
  end

  -- pan position line
  ctx:set_line_width(1)
  ctx:set_source_rgba(0.665, 0.351, 0.163, 1.0)
  local centerx = -.5 + round(panx)
  ctx:move_to (centerx, bary - 4)
  ctx:line_to (centerx, bary + barh + 3)
  ctx:stroke()

  -- stereo width bar
  if panWidth > 0 then
    local x1 = -.5 + round(barx)
    local x2 = centerx -- -.5 + round(barx + barw)
    local y1 = -.5 + bary
    local w = x2 - x1
    local x3 = x2 + w

    ctx:rectangle(x1, y1, w, barh)
    ctx:set_source_rgba(0.565, 0.251, 0.063, 1.0)
    ctx:fill()

    ctx:rectangle(x2, y1, w, barh)
    ctx:set_source_rgba(0.565, 0.251, 0.063, 1.0)
    ctx:fill()

    ctx:rectangle(x1, y1, w, barh)
    ctx:set_source_rgba(0.665, 0.351, 0.163, 1.0)
    ctx:stroke()

    ctx:rectangle(x2, y1, w, barh)
    ctx:set_source_rgba(0.665, 0.351, 0.163, 1.0)
    ctx:stroke()
  end

  return {w, h}
end
