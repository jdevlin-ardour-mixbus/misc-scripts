ardour {
  ["type"]    = "dsp",
  name        = "JD Swap L/R",
  category    = "utility spatial",
  license     = "MIT",
  author      = "John Devlin",
  description = [[Swap L and R channels.]],
  version     = 2.0
}

-- return possible i/o configurations
function dsp_ioconfig ()
  return { [1] = { audio_in = 2, audio_out = 2}, }
end

function dsp_params ()
  return {
    { ["type"] = "input", name = "Swapped", min = 0, max = 1, default = 1, toggled = true },
    { ["type"] = "input", name = "Speed (ms)", min = 1, max = 1000, default = 5 },
   }
end

local transitionSamplesStillToProcess = 0
local isb = nil -- memory input scratch buffer
local gtb = nil -- memory gain transition buffer
local mb = { nil, nil } -- memory mix buffers
local sample_rate

local params = {
  count = 2,  -- number of params
  prevValues = {},
  swapped = true,
  transitionSamples = 0 -- gain transition duration in samples (calculated later based on "speed" param and sample rate)
}

-- current and target gains: [1][1] = L in L out, [1][2] = L in R out, [2][1] = R in L out, [2][2] = R in R out
local curGains = { { 1, 0 }, { 0, 1 }}
local targetGains = { { 1, 0 }, { 0, 1 }}

function dsp_init (rate)

  sample_rate = rate

  -- allocate buffers
  isb = ARDOUR.DSP.DspShm(8192)
  gtb = ARDOUR.DSP.DspShm(8192)
  mb[1] = ARDOUR.DSP.DspShm(8192)
  mb[2] = ARDOUR.DSP.DspShm(8192)
  isb:clear()
  gtb:clear()
  mb[1]:clear()
  mb[2]:clear()
end

function dsp_configure (ins, outs)
end

-- the DSP callback function
function dsp_run (ins, outs, n_samples)

  assert(n_samples <= 8192) -- ensure the number of samples is not larger than our buffers

  local paramsChanged = getParams()

  if paramsChanged then
    calcTargetGains()
    transitionSamplesStillToProcess = params.transitionSamples
  end

  if transitionSamplesStillToProcess <= 0 then

    -- no transition required

    if params.swapped then

      if ins[1] == outs[1] or ins[2] == outs[2] then
        ARDOUR.DSP.copy_vector(mb[1]:to_float(0), ins[1]:offset(0), n_samples)
        ARDOUR.DSP.copy_vector(mb[2]:to_float(0), ins[2]:offset(0), n_samples)
        ARDOUR.DSP.copy_vector (outs[1]:offset(0), mb[2]:to_float(0), n_samples)
        ARDOUR.DSP.copy_vector (outs[2]:offset(0), mb[1]:to_float(0), n_samples)
      else
        ARDOUR.DSP.copy_vector (outs[1]:offset(0), ins[2]:offset(0), n_samples)
        ARDOUR.DSP.copy_vector (outs[2]:offset(0), ins[1]:offset(0), n_samples)
      end

    else

      for c = 1, 2 do
        if ins[c] ~= outs[c] then
          ARDOUR.DSP.copy_vector (outs[c]:offset(0), ins[c]:offset(0), n_samples)
        end
      end

    end

  else

    -- transition: crossfade L -> R and R -> L

    for cin = 1, 2 do

      -- L out...
      ARDOUR.DSP.copy_vector(isb:to_float(0), ins[cin]:offset(0), n_samples)
      transitionGainChange(cin, 1, n_samples)
      if cin == 1 then
        ARDOUR.DSP.copy_vector(mb[1]:to_float(0), isb:to_float(0), n_samples)
      else
        ARDOUR.DSP.mix_buffers_no_gain(mb[1]:to_float(0), isb:to_float(0), n_samples)
      end

      -- R out...
      ARDOUR.DSP.copy_vector(isb:to_float(0), ins[cin]:offset(0), n_samples)
      transitionGainChange(cin, 2, n_samples)
      if cin == 1 then
        ARDOUR.DSP.copy_vector(mb[2]:to_float(0), isb:to_float(0), n_samples)
      else
        ARDOUR.DSP.mix_buffers_no_gain(mb[2]:to_float(0), isb:to_float(0), n_samples)
      end

    end

    ARDOUR.DSP.copy_vector(outs[1]:offset(0), mb[1]:to_float(0), n_samples)
    ARDOUR.DSP.copy_vector(outs[2]:offset(0), mb[2]:to_float(0), n_samples)

    -- The transition might not yet be complete so store where we're up to.
    -- It might have processed fewer than n_samples but that's okay - if it did then the transition is
    -- finished anyway. It will result in transitionSamplesStillToProcess being < 0 and that too is okay.
    transitionSamplesStillToProcess = transitionSamplesStillToProcess - n_samples

  end

end

function getParams()

  -- Get the parameters from the controls and store them in params.
  -- Returns true if any parameter does not match its current value in params, i.e. it has changed since this was last called.

  local ctrl = CtrlPorts:array() -- get control port array
  local changed = false

  for c = 1, params.count do
    if ctrl[c] ~= params.prevValues[c] then changed = true end
    params.prevValues[c] = ctrl[c]
  end

  params.swapped = ctrl[1] > 0
  params.transitionSamples = math.floor((sample_rate / 1000) * ctrl[2])

  return changed
end

function calcTargetGains()

  if params.swapped then
    targetGains = { { 0, 1 }, { 1, 0 }}
  else
    targetGains = { { 1, 0 }, { 0, 1 }}
  end

end

function transitionGainChange(cin, cout, n_samples)

  -- Apply gain transition to isb buffer. When parameters change, transition from the current gain (gfrom)
  -- to the target gain (gto) over a period of time (transitionMs) for smoother results.

  if n_samples == 0 then return end
  if transitionSamplesStillToProcess == 0 then return end

  local gfrom = curGains[cin][cout]
  local gto = targetGains[cin][cout]

  local steps = math.min(transitionSamplesStillToProcess, n_samples)

  local adjPerStep = (gto - gfrom) / transitionSamplesStillToProcess

  local a = {}

  for i = 1, steps do
    a[i] = gfrom + (adjPerStep * i)
  end

  -- If n_samples < transitionSamplesStillToProcess then we didn't have enough samples this round to
  -- reach the target gain. So the current gain is wherever we got up to.
  local gcur = gto
  if n_samples < transitionSamplesStillToProcess then
    gcur = a[n_samples]
  end

  -- If n_samples > transitionSamplesStillToProcess then a[1]..a[steps] contain the completed transition
  -- and everything after that is at the target gain
  for i = steps + 1, n_samples do
    a[i] = gto
  end

  -- store the multipliers in gtb buffer
  gtb:to_float(0):set_table(a, n_samples)

  -- apply the gtb buffer to the the input scratch buffer, i.e. apply the gain multipliers to isb
  ARDOUR.DSP.mmult(isb:to_float(0), gtb:to_float(0), n_samples)

  --store the current gain
  curGains[cin][cout] = gcur

end
