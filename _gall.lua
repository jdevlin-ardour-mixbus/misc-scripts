--[[
Gallant attempt at an Ardour Lua Library (Gaaaall .. Gall for short).

Version: 3.0

Adds a Gall global variable containing functions, designed in an object-oriented fashion.
--]]

Gall = {}


-- *****************************************************************************
-- Gall.ArdourObject
-- *****************************************************************************

---Base class for Gall classes that wrap Ardour objects. Not intended to be used directly.
---@class Gall.ArdourObject
---@field ao any @The Ardour object being wrapped
---@field gallType string @The name of this Gall.ArdourObject class or subclass
Gall.ArdourObject = {
  ao = nil,
  gallType = "Gall.ArdourObject"
}

---Intended for internal use only. Use the subclass `get()` method instead, e.g. to create a Gall.Route 
---object use `Gall.Routes:get()`. `newInstance()` alone is not enough to work correctly for some subclasses.
---Also, the `get()` methods have the correct annotations/comments for the data types specific to that subclass.
---
---Constructor: creates a new Gall.ArdourObject class/subclass instance to wrap the specified Ardour object.
---@param obj any @The Ardour object to wrap, or a name by which to find it in Ardour, or another Gall.ArdourObject class/subclass instance.
---@return any @The new instance, or `obj` if that is a Gall.ArdourObject class/subclass instance, or `nil` if `obj` is any of:  `nil` | not the correct Ardour type | not a name of an Ardour object of the correct type.
function Gall.ArdourObject:newInstance(obj)

  if not obj then return end
  if obj.gallType then return obj end  -- is already a Gall object: just reuse it
  local objAo = self:toAo(obj)
  if not objAo then return end  -- no such Ardour object; this Gall.ArdourObject should be nil too as it doesn't wrap anything, so return nil

  local newObj = { ao = objAo }
  setmetatable(newObj, self)
  self.__index = self
  return newObj
end

---Intended for internal use only. Provides a mechanism to define a new Gall.ArdourObject subclass.
---@param subclassGallType string @The subclass name
---@return table @The new subclass
function Gall.ArdourObject:newSubclass(subclassGallType)
  local obj = { gallType = subclassGallType }
  setmetatable(obj, self)
  self.__index = self
  return obj
end

---Intended for internal use only. Called by `newInstance()`, this provides a mechanism for
---subclasses to find the Ardour object in question and verify that it is of the expected type.
---@param obj any @An Ardour object, name of an Ardour object, or a Gall.ArdourObject
---@return any @The Ardour object that `obj` represents. `nil` if not found or not the correct type.
function Gall.ArdourObject:toAo(obj)
  if obj.gallType then
    return obj.ao
  elseif type(obj) == "string" then
    return self:findAoByName(obj)
  end
  --is an Ardour object: assert it is the correct type
  if not self:isCorrectAoType(obj) then return end
  return obj
end

---Intended for internal use only. Subclasses implement this to find the Ardour object (of the type specific to them) with this name.
---@param name string @Name of the Ardour object to find
---@return any @The Ardour object found. `nil` if not found.
function Gall.ArdourObject:findAoByName(name)
  return nil
end

---Intended for internal use only. Subclasses implement this to verify that this Ardour object is of the type that they wrap.
---@param ardourObject any @The Ardour object to verify
---@return boolean @Verification success
function Gall.ArdourObject:isCorrectAoType(ardourObject)
  return true
end



-- *****************************************************************************
-- Gall.Route
-- *****************************************************************************

---Wraps an ARDOUR.Route.
---@class Gall.Route : Gall.ArdourObject
---@field ao ARDOUR.Route
---@field private internalMbNumber integer @DO NOT USE. This is for internal use only. Use `mbGetNumber()` instead.
Gall.Route = Gall.ArdourObject:newSubclass("Gall.Route")

Gall.Route.internalMbNumber = nil


--some ARDOUR.PresentationInfo.Flag values not included in documentation
Gall.Route.PresentationInfo = {
  Flags = {
    Mixbus = 0x1000,
    FoldbackBus = 0x2000
  }
}

function Gall.Route:findAoByName(name)
  local r = Session:route_by_name(name)
  if not r or r:isnil() then return nil end
  return r
end

function Gall.Route:isCorrectAoType(ardourObject)
  return ardourObject:to_route()
end

--- Returns the type of route it is, "audio" or "midi", based on the kind of input ports it has.
--- Midi routes can also have audio output ports, so while this function does indicate the kind of input it
--- has, it does not necessarily indicate the kind of outputs it has.
---@return string
function Gall.Route:getDataType()
  -- was using self.ao:data_type():to_string() here, which gave "midi" or "audio", but was found that Midi Busses always said "audio"
  return Gall.Util:iif(self.ao:input():n_ports():n_midi() == 0, "audio", "midi")
end

--- Shorthand for `getDataType() == "audio"`. Indicates that this route is an audio track/bus as it has audio inputs.
---@return boolean
function Gall.Route:isAudio()
  return self:getDataType() == "audio"
end

--- Shorthand for `getDataType() == "midi"`. Indicates that this route is a MIDI track/bus as it has MIDI inputs.
---@return boolean
function Gall.Route:isMidi()
  return self:getDataType() == "midi"
end

---Is a track.
---@return boolean
function Gall.Route:isTrack()
  return not self.ao:to_track():isnil()
end

---Is the Monitor route.
---@return boolean
function Gall.Route:isMonitor()
  return self.ao:is_monitor()
end

---Is the Master route.
---@return boolean
function Gall.Route:isMaster()
  return self.ao:is_master()
end

function Gall.Route:isFoldback()
  local fbFlag = Gall.Route.PresentationInfo.Flags.FoldbackBus
  return bit32.band(self.ao:presentation_info_ptr():flags(), fbFlag) == fbFlag
end

---Is one of the 'special' kinds of routes: Monitor, Master or Mixbus.
---@return boolean
function Gall.Route:isSpecial()
  return self.ao:is_monitor() or self.ao:is_master() or self:isFoldback() or self:isMb()
end

---Is a Mixbus.
---@return boolean
function Gall.Route:isMb()
  --return Gall.Routes:getMixbusForName(self.ao:name()) ~= nil
  local mbFlag = Gall.Route.PresentationInfo.Flags.Mixbus
  return bit32.band(self.ao:presentation_info_ptr():flags(), mbFlag) == mbFlag
end

---Is a Mixbus of number 1 to 8.
---@return boolean
function Gall.Route:isMb1to8()
  local number = self:mbGetNumber()
  return number >= 1 and number <= 8
end

---Is a Mixbus of number 9 to 12.
---@return boolean
function Gall.Route:isMb9to12()
  return self:mbGetNumber() >= 9
end

---Has two audio input ports.
---@return boolean
function Gall.Route:isStereo()
  return self.ao:input():n_ports():n_audio() == 2
end

---Has only one audio input port.
---@return boolean
function Gall.Route:isMono()
  return self.ao:input():n_ports():n_audio() == 1
end

--- Converts this route to stereo, i.e. adds an input port (not connected). Only if it's currently mono, else this does nothing.
function Gall.Route:makeStereo()
  if self:isMono() then
    self.ao:input():add_port("", nil, ARDOUR.DataType("audio"))
  end
end

--- Converts this route to mono, i.e. removes the second input port. Only if it's currently stereo, else this does nothing.
function Gall.Route:makeMono()
  if self:isStereo() then
    self.ao:input():remove_port(self.ao:input():audio(1), nil)
  end
end

--- Returns the group this route belongs to, as a Gall.Group.
---@return Gall.Group @The group. `nil` if not in a group.
function Gall.Route:getGroup()
  for g in Session:route_groups():iter() do
    for route in g:route_list():iter() do
      if route:name() == self.ao:name() then
        return Gall.Groups:get(g)
      end
    end
  end
end

---Gets the Inserts of this route. (Not PluginInserts.)
---@return Gall.List.Processor @The inserts. Empty list if none found.
function Gall.Route:getInserts()

  local ls = Gall.List.Processor:new()

  --There doesn't seem to be a direct way to determine if a processor is an Insert. to_insert() is not it.
  --The following combination of checks seems to be the only way to determine:
  -- (not to_ioprocessor():isnil()) and (to_send():isnil()) and (display_to_user())

  for p in self:getProcessors():iter() do
    if not p.ao:to_ioprocessor():isnil() and p.ao:to_send():isnil() and p.ao:display_to_user() then
      ls:add(p)
    end
  end

  return ls
end

---Gets the InternalSends of this route.
---@return Gall.List.InternalSend @The sends. Empty list if it has none.
function Gall.Route:getInternalSends()
  local grs = Gall.List.InternalSend:new()
  local i = 0
  repeat
    local send = self.ao:nth_send(i)
    if not send:isnil() then
      local is = send:to_internalsend()
      if not is:isnil() then
        grs:add(Gall.InternalSends:get(is))
      end
    end
    i = i + 1
  until send:isnil()
  return grs
end

---Gets the InternalSend of this route that is to the specified route.
---@param toRoute Gall.Route | ARDOUR.Route | string @The to-route, as either an object or a name
---@return Gall.InternalSend @The send. `nil` if no such send exists.
function Gall.Route:getInternalSendTo(toRoute)
  toRoute = Gall.Routes:get(toRoute)
  if not toRoute then return end
  for gis in self:getInternalSends():iter() do
    if gis:getTarget().ao == toRoute.ao then
      return gis
    end
  end
end


---Create an internal send to this route from one or more other routes.
---@param fromRoutes Gall.List.Route @The from-routes
---@param gain number @The gain for the new sends. As per `Gall.InternalSend:setGain()`.
---@param preFader boolean @If true then the sends are created as pre-fader
---@return Gall.List.InternalSend @The created sends. Empty list if none created.
function Gall.Route:createInternalSendsFrom(fromRoutes, gain, preFader)
  local newSends = Gall.List.InternalSend:new()
  for r in fromRoutes:iter() do
    newSends:add(r:createInternalSendTo(self, gain, preFader))
  end
  return newSends
end

---Create an internal send from this route to another route.
---@param toRoute Gall.Route | ARDOUR.Route | string @The to-route, as either an object or a name
---@param gain number @The gain for the new send. As per `Gall.InternalSend:setGain()`.
---@param preFader boolean @If true then the send is created as pre-fader
---@return Gall.InternalSend @The created send
function Gall.Route:createInternalSendTo(toRoute, gain, preFader)
  toRoute = Gall.Routes:get(toRoute)
  local tracks = ARDOUR.RouteListPtr()
  tracks:push_back(self.ao)
  local placement = ARDOUR.Placement.PostFader
  if preFader then placement = ARDOUR.Placement.PreFader end
  Session:add_internal_sends(toRoute.ao, placement, tracks)
  local send = self:getInternalSendTo(toRoute)
  if gain then
    send:setGain(gain)
  end
  return send
end

---Finds the internal send targeting `oldTarget`, removes it and creates a new send targeting `newTarget`, in
-- the same position and with the same gain as the original.
---@param oldTarget Gall.Route | ARDOUR.Route | string @The current to-route, as either an object or a name
---@param newTarget Gall.Route | ARDOUR.Route | string @The new to-route, as either an object or a name
---@return Gall.InternalSend @The send. `nil` if no such send was found or the new target was not found.
function Gall.Route:replaceInternalSend(oldTarget, newTarget)

  local s = self:getInternalSendTo(oldTarget)
  if not s then return nil end

  local t = Gall.Routes:get(newTarget)
  if not t then return nil end

  local gain = s.ao:gain_control():get_value() --get the current send's gain
  Session:add_internal_send(t.ao, s.ao, self.ao)  -- 2nd argument = "the processor to position it before". For last position use route:main_outs()
  local newSend = self:getInternalSendTo(t.ao) -- get the send just created
  newSend.ao:gain_control():set_value(gain, PBD.GroupControlDisposition.NoGroup) -- set the gain the same as the old send
  self.ao:remove_processor(s.ao, nil, true) -- remove the old send

  return newSend
end

---Gets this route's Mixbus sends.
---@param activeOnly boolean @If true then only active sends are included
---@return Gall.List.MbSend @The sends. Empty list if none found.
function Gall.Route:getMbSends(activeOnly)
  
  local ls = Gall.List.MbSend:new()

  --no sends in Ardour, MBs 9..12, Master, Monitor or Foldback strips
  if Gall.App:isArdour() or self:isMb9to12() or self:isMaster() or self:isMonitor() or self:isFoldback() then
    return ls
  end

  -- this route must be a track, bus or MB 1..8

  if not self:isMb1to8() then  -- no 1..8 sends on MB 1..8
    for i = 1, 8 do
      local mbs = Gall.MbSends:get(self, i)
      if mbs and (not activeOnly or mbs:isActive()) then
        ls:add(mbs)
      end
    end
  end

  if Gall.App:is32c() then  -- if 32c then sends 9..12 on tracks, busses and MBs 1..8
    for i = 9, 12 do
      local mbs = Gall.MbSends:get(self, i)
      if mbs and (not activeOnly or mbs:isActive()) then
        ls:add(mbs)
      end
    end
  end

  return ls
end

---Gets the Mixbus send on this route with the specified number.
---@param number number @The send number, starting from 1
---@return Gall.MbSend @The send. `nil` if not found.
function Gall.Route:getMbSend(number)
  return Gall.MbSends:get(self, number)
end

---Deactivates all Mixbus sends on this route.
function Gall.Route:deactivateAllMbSends()
  for mbs in self:getMbSends(true):iter() do mbs:setActive(false) end
end

--- Returns true if the "master" send button is active on this route.
---@return boolean
function Gall.Route:isMasterSendActive()
  return self.ao:master_send_enable_controllable():get_value() == 1
end

--- Activates/decativates the "master" send button on this route. Does not affect other routes in its group.
---@param active boolean @If true then activates, else deactivates
function Gall.Route:setMasterSendActive(active)
  self.ao:master_send_enable_controllable():set_value(Gall.Util:iif(active, 1, 0), PBD.GroupControlDisposition.NoGroup)
end

--- Gets the RouteTimeAxisView of this route.
---@return ArdourUI.RouteTimeAxisView
function Gall.Route:toRtav()
  return Editor:rtav_from_route(self.ao)
end

--- Gets the TimeAxisView of this route.
---@return ArdourUI.TimeAxisView
function Gall.Route:toTav()
  return Editor:rtav_from_route(self.ao):to_timeaxisview()
end

---Gets the VCAs that this route is a slave to.
---@return Gall.List.Vca @The VCAs. Empty list if none found.
function Gall.Route:getVcaMasters()
  return Gall.List.Vca:new(self.ao:to_slavable():masters(Session:vca_manager()))
end

--- Selects this route.
---
--- NOTE: It deselects all other routes, therefore cannot be used multiple times for selecting multiple routes at once.
---@param scrollIntoView boolean @If true (or `nil`) then it also scrolls the route into view
---@param unhide boolean @If the route is hidden then this function does nothing unless this parameter is true (or `nil`) in which case it unhides the route. NOTE: it will not unhide Mixbusses.
---@return boolean @True if the route was selected, else false.
function Gall.Route:select(scrollIntoView, unhide)

  local sels = Gall.Routes:getSelected()

  local selfIsSelected = self.ao:is_selected()
  local selfIsSoleSelection = sels:size() == 1 and selfIsSelected

  if selfIsSoleSelection then
    -- Self is the only ROUTE selected but VCAs might also be selected. Gall.routes:getSelected() does not know about VCAs.
    -- If any VCAs are selected then selfIsSoleSelection is not true.
    for vca in Session:vca_manager():vcas():iter() do
      if vca:is_selected() then
        selfIsSoleSelection = false
        break
      end
    end
  end

  local selectionDone = false

  if not selfIsSoleSelection then

    if self.ao:is_hidden() then
      if unhide ~= nil and unhide ~= true then
        return false
      end
      if self:isMb() then
        print("Gall.Route:select(): cannot unhide as this is a Mixbus.")
        return false
      end
      self:setHidden(false, false)
    end

    --[[
      We could just do a "Mixer, select-none" action here, forcing a "select-next-route" loop to begin
      at route 1. Indeed, that is the fallback operation here. But selection is slow, so better to iterate 
      over as few routes as possible when looking for the route in question.

      The starting position of "select-next[prev]-route" is the most-recently-added-to-the-selection route, 
      which should be the last route in the {sels} list. We can use that to see if we can do anything 
      involving fewer selection actions.
    ]]

    if sels:size() == 0 then
      --there is no selection
    else
      local posSelf = self:getPosition()
      local posLastSel = sels:back():getPosition()

      if posSelf == posLastSel then
        --[[
          This self route is the most-recently-selected route. Just do a previous-then-next. This will
          re-select it and clear the other selections in the process.
          However, this does not work if the previous-visible route is also currently selected. In that 
          case do a next-then-previous instead, but that likewise does not work if the next-visible route 
          is also currently selected.
        ]]
        if not self:getPreviousVisibleRoute().ao:is_selected() then
          Editor:access_action("Editor", "select-prev-route")
          Editor:access_action("Editor", "select-next-route")
          selectionDone = self.ao:is_selected()
        elseif not self:getNextVisibleRoute().ao:is_selected() then
          Editor:access_action("Editor", "select-next-route")
          Editor:access_action("Editor", "select-prev-route")
          selectionDone = self.ao:is_selected()
        end

      else
        --[[
          This self route is NOT the most-recently-selected (MSR) route, and may or may not be selected itself.
          If MSR is to the left of self then we can do a "select-next-route" loop until we hit self, or a
          "select-prev-route" if MSR is to the right. However, this only works if there are non-selected visible 
          routes between MSR and self, or self is non-selected.
        ]]

        local canDoThis = false

        if not selfIsSelected then
          canDoThis = true
        else
          local firstPosToCheck = posSelf + 1
          local lastPosToCheck = posLastSel - 1
          if posSelf > posLastSel then
            firstPosToCheck = posLastSel + 1
            lastPosToCheck = posSelf - 1
          end

          for pos = firstPosToCheck, lastPosToCheck do
            local r = Gall.Routes:getByPosition(pos)
            -- VCAs are not routes so will be nil here; need to test for that
            if r and not r.ao:is_hidden() and not r.ao:is_selected() then
              -- found a visible non-selected route between MSR and self: that's all we need to know
              canDoThis = true
              break
            end
          end
        end

        if canDoThis then
          repeat
            if posSelf < posLastSel then
              Editor:access_action("Editor", "select-prev-route")
            else
              Editor:access_action("Editor", "select-next-route")
            end
          until self.ao:is_selected()
          selectionDone = true
        end

      end

    end

    if not selectionDone then
      -- just do a "select-next-route" loop starting from route 1
      Editor:access_action("Mixer", "select-none")
      repeat
        Editor:access_action("Editor", "select-next-route")
      until self.ao:is_selected()
    end

  end

  if scrollIntoView == nil or scrollIntoView == true then
    self:scrollIntoView()
  end

  return true
end

--- Scrolls the mixer's route pane such that this route is visible. Typically this will make it the left-most visible
--- route but only if the available scrolling range allows it.
---@param unhide boolean @If the route is hidden then this function does nothing unless this parameter is true (or `nil`) in which case it unhides the route
function Gall.Route:scrollIntoView(unhide)

  if self.ao:is_hidden() then
    if unhide ~= nil and unhide ~= true then return end
    Editor:show_track_in_display (self:toTav(), true)
  end

  for i = 1, Session:get_stripables():size() do
    Editor:access_action("Mixer", "scroll-left")
  end

  local pos = self:getPosition()
  local numVisibleBeforeThis = 0

  for r in Gall.Routes:getAll():iter() do
    if r:getPosition() < pos and not r.ao:is_hidden() then
      numVisibleBeforeThis = numVisibleBeforeThis + 1
    end
  end

  for i = 1, numVisibleBeforeThis do
    Editor:access_action("Mixer", "scroll-right")
  end

end

--- Returns the most-adjacent visible route that is positioned to the right of this route.
--- If there are none then the Master route is returned instead.
--- I.e. If this route was selected and then a select-next-route action was performed, what
--- route would now be selected?
---@return Gall.Route
function Gall.Route:getNextVisibleRoute()

  local rs = Gall.Routes:getAll(true, false, true, true, false, true)

  local foundThis = false

  for r in rs:iter() do
    if r.ao == self.ao then
      foundThis = true
    else
      if foundThis and r.ao:is_hidden() == false then
        return r
      end
    end
  end

  -- We should not get to here because it would have found the always-visible Master route at least.
  -- For sanity's sake just return the Master.
  return Gall.Routes:getMaster()
end

--- Returns the most-adjacent visible route that is positioned to the left of this route.
--- If there are none then the Master route is returned instead.
--- I.e. If this route was selected and then a select-prev-route action was performed, what
--- route would now be selected?
---@return Gall.Route
function Gall.Route:getPreviousVisibleRoute()

  if self:isMonitor() then
    return Gall.Routes:getMaster()
  end

  local rs = Gall.Routes:getAll(true, true, true, true, false, true) --sorting is desc order

  local foundThis = false

  for r in rs:iter() do
    if r.ao == self.ao then
      foundThis = true
    else
      if foundThis and r.ao:is_hidden() == false then
        return r
      end
    end
  end

  -- If we got to here then there are no visible routes prior to this route.
  -- So we would start searching from the end, which would be the Master route, but
  -- that one is always visible anyway, so just return it.
  return Gall.Routes:getMaster()
end

---Hide/unhide this route. NOTE: does not work on Mixbusses.
---@param hide boolean @If true then hide, else unhide
---@param moveIntoViewUponShow boolean @If true then move into view after showing. Default = false. Ignored when `hide` = true.
function Gall.Route:setHidden(hide, moveIntoViewUponShow)
  if hide then
    Editor:hide_track_in_display(self:toTav(), false) -- 2nd arg = "only if selected"
  else
    Editor:show_track_in_display(self:toTav(), moveIntoViewUponShow)
  end
end

---Get this route's ports.
---@param inOut string @'"in" for input ports only, "out" for output ports only, `nil` for both'
---@param audioMidi string @'"midi" for midi ports only, "audio" for audio ports only, `nil` for both'
---@return Gall.List.Port @The ports. Empty list if none found.
function Gall.Route:getPorts(inOut, audioMidi)
  local ls = Gall.List.Port:new()
  if not inOut or inOut == "in" then
    local io = self.ao:input()
    if not audioMidi or audioMidi == "audio" then
      for i = 0, io:n_ports():n_audio() - 1 do
        ls:add(Gall.Port:newInstance(io:audio(i)))
      end
    end
    if not audioMidi or audioMidi == "midi" then
      for i = 0, io:n_ports():n_midi() - 1 do
        ls:add(Gall.Port:newInstance(io:midi(i)))
      end
    end
  end
  if not inOut or inOut == "out" then
    local io = self.ao:output()
    if not audioMidi or audioMidi == "audio" then
      for i = 0, io:n_ports():n_audio() - 1 do
        ls:add(Gall.Port:newInstance(io:audio(i)))
      end
    end
    if not audioMidi or audioMidi == "midi" then
      for i = 0, io:n_ports():n_midi() - 1 do
        ls:add(Gall.Port:newInstance(io:midi(i)))
      end
    end
  end
  return ls
end

-- Disconnects all output ports on this route.
function Gall.Route:disconnect()
  for p in self:getPorts("out"):iter() do p.ao:disconnect_all() end
end

---Removes all direct connections between the output ports on this route and the input ports on `toRoute`.
---@param toRoute ARDOUR.Route | string | Gall.Route @The to-route that this route is connected to, as an object or a name
function Gall.Route:disconnectFromRoute(toRoute)
  local target = Gall.Routes:get(toRoute)
  if not target then return end
  local dataType = target:getDataType()
  local selfOutPorts = self:getPorts("out", dataType)
  local targetInPorts = target:getPorts("in", dataType)
  local maxPortCount = math.max(selfOutPorts:size(), targetInPorts:size())
  for i = 0, selfOutPorts:size() - 1 do
    for j = 0, targetInPorts:size() -1 do
      if dataType == "midi" then
        self.ao:output():midi(i):disconnect(target.ao:input():midi(j):name())
      else
        self.ao:output():audio(i):disconnect(target.ao:input():audio(j):name())
      end
    end
  end
end

---WARNING: direct connections can cause an "Ambiguous" latency warning.
---
---Connects the output ports of this route to the input ports of `toRoute`. The port type (audio/midi) is the data type of `toRoute`.
---@param toRoute ARDOUR.Route | string | Gall.Route @The route to connect to, as an object or a name
---@param disconnectFirst boolean @If true (default) then all thisRoute-to-toRoute connections are removed first
function Gall.Route:connectToRoute(toRoute, disconnectFirst)
  if disconnectFirst == nil or disconnectFirst then self:disconnect() end
  local target = Gall.Routes:get(toRoute)
  if not target then return end
  local dataType = target:getDataType()
  local selfOutPorts = self:getPorts("out", dataType)
  local targetInPorts = target:getPorts("in", dataType)
  local maxPortCount = math.max(selfOutPorts:size(), targetInPorts:size())
  for i = 0, maxPortCount - 1 do
    local idxSelf = math.min(i, selfOutPorts:size() - 1)
    local idxTarget = math.min(i, targetInPorts:size() - 1)
    if dataType == "midi" then
      self.ao:output():midi(idxSelf):connect(target.ao:input():midi(idxTarget):name())
    else
      self.ao:output():audio(idxSelf):connect(target.ao:input():audio(idxTarget):name())
    end
  end
end

---Gets all routes whose input ports are directly connected to this route's output ports.
---@return Gall.List.Route @The routes. Empty list if none found.
function Gall.Route:getConnectedTo()
  local ls = Gall.List.Route:new()
  local selfOutPorts = self:getPorts("out")
  for r in Gall.Routes:getAll(false, false, true):iter() do  -- for each route
    for rip in r:getPorts("in"):iter() do     -- for each input port on that route
      local rpName = rip.ao:name()
      for sop in selfOutPorts:iter() do  -- for each output port on this route
        if sop.ao:connected_to(rpName) then
          ls:addUnique(Gall.Route:newInstance(r))
        end
      end
    end
  end
  return ls
end

---Returns true if there is any direct connection between the output ports of this route and the input ports of `toRoute`.
---@param toRoute ARDOUR.Route | string | Gall.Route @The route to check, as an object or a name
---@return boolean
function Gall.Route:isConnectedTo(toRoute)
  return self:getConnectedTo():contains(Gall.Routes:get(toRoute))
end

-- Returns strip position (presentation order), 0 indexed

---Gets the strip position (presentation order) of this route. 0-based; the first position is 0 not 1.
---@return number
function Gall.Route:getPosition()
  return self.ao:presentation_info_ptr():order()
end

---Sets the strip position (presentation order). Does nothing if this route is 'special' (monitor, master, foldback or MB).
---@param newPos number @0-based; the first position is 0 not 1.
function Gall.Route:setPosition(newPos)
  if self:isSpecial() then return end
  local oldPos = self:getPosition()
  -- constrain to allowed range
  local allNonSpecialRoutes = Gall.Routes:getAll()
  local maxAllowedPos = allNonSpecialRoutes:size() -1  --
  if newPos > maxAllowedPos then newPos = maxAllowedPos end
  if newPos < 0 then newPos = 0 end
  --make the change
  self.ao:set_presentation_order(newPos)
  --adjust all others in affected range. Must do this, does not happen automatically.
  local affectedRangeFrom = oldPos
  local affectedRangeTo = newPos
  local adjust = -1
  if newPos < oldPos then
    affectedRangeFrom = newPos
    affectedRangeTo = oldPos
    adjust = 1
  end
  for r in allNonSpecialRoutes:iter() do
    local position = r:getPosition()
    if position >= affectedRangeFrom and position <= affectedRangeTo and r.ao ~= self.ao then
      r.ao:set_presentation_order(position + adjust)
    end
  end
end

---Gets all processors of this route.
---@return Gall.List.Processor @The processors. Will never be empty as all routes have at least a "Fader".
function Gall.Route:getProcessors()

  local ls = Gall.List.Processor:new()
  local i = 0
  repeat
    local p = self.ao:nth_processor(i)
    if not p:isnil() then
      ls:add(Gall.Processors:get(p, self))
    end
    i = i + 1
  until p:isnil()
  return ls
end

---Gets all processors with this name.
---@param name string
---@param isDisplayName boolean @If true then searches by `display_name()` instead of `name()`
---@return Gall.List.Processor @The processors. Empty list if none found.
function Gall.Route:getProcessorsForName(name, isDisplayName)

  local ls = Gall.List.Processor:new()
  for p in self:getProcessors():iter() do
    if (isDisplayName and p.ao:display_name() == name) or (not isDisplayName and p.ao:name() == name) then
      ls:add(p)
    end
  end
  return ls
end

---Gets the index of the given processor on this route.
---@param processor Gall.Processor | ARDOUR.Processor @The processor
---@return integer @The index. `nil` if not found.
function Gall.Route:getProcessorIndex(processor)

  local proc = Gall.Processors:get(processor, self)
  if proc then
    local i = 0
    repeat
      local p = self.ao:nth_processor(i)
      if not p:isnil() and p == proc.ao then return i end
      i = i + 1
    until p:isnil()
  end
end

---Gets the index of the "Fader" processor on this route.
---@return integer
function Gall.Route:getFaderProcessorIndex()
  return self:getProcessorIndex(self:getProcessorsForName("Fader", true):getSoleObject())
end

---Gets the PluginInserts of this route.
---@return Gall.List.Processor @The inserts. Empty list if none found.
function Gall.Route:getPluginInserts()

  local ls = Gall.List.Processor:new()
  for p in self:getProcessors():iter() do
    if not p.ao:to_plugininsert():isnil() then
      ls:add(p)
    end
  end
  return ls
end

---Returns true if the given processor is after the fader on this route.
---@param processor Gall.Processor | ARDOUR.Processor @The processor
---@return boolean @False if processor is pre-fade or not found
function Gall.Route:isProcessorPostFade(processor)
  local idx = self:getProcessorIndex(processor)
  local fidx = self:getFaderProcessorIndex()
  return idx and fidx and idx > fidx
end

---Gets the PluginInserts of this route with the given name.
---@param name string
---@return Gall.List.Processor @The inserts. Empty list if none found.
function Gall.Route:getPluginInsertsForName(name)

  local ls = Gall.List:newInstance()
  for p in self:getProcessors():iter() do
    if not p.ao:to_plugininsert():isnil() and p.ao:name() == name then
      ls:add(p)
    end
  end
  return ls
end

---Removes the processor from this route.
---
---If `processor` is a name and multiple processors exist with this name then all are removed.
---@param processor Gall.Processor | ARDOUR.Processor | string @The processor(s) to remove, by object or name
---@param isDisplayName boolean @If `processor` is a string then `true` searches by `display_name()` instead of `name()`. Ignored if `processor` is not a string.
---@return boolean @True if (all) successfully removed
function Gall.Route:removeProcessor(processor, isDisplayName)

  local ls = Gall.List.Processor:new()
  if type(processor) == "string" then
    ls:addIter(self:getProcessorsForName(processor, isDisplayName))
  else
    ls:add(Gall.Processors:get(processor, self))
  end

  local anyFailed = false

  for p in ls:iter() do
    if self.ao:remove_processor(p.ao, nil, true) ~= 0 then
      anyFailed = true
    end
  end

  return not anyFailed
end

---Adds a plugin to this route.
---@param pluginName string @Name of the plugin
---@param pluginType ARDOUR.PluginType @E.g. ARDOUR.PluginType.LV2, ARDOUR.PluginType.LXVST
---@param pluginPreset string @Preset name. Optional.
---@param index number @The position to place it. 0-based. -1 = last. Optional: default = -1. Resulting position might differ, e.g. position 0 is reserved for the PRE processor in Mixbus so the actual position would be 1.
---@param activate boolean @Activate the plugin. Optional; default = true.
---@return Gall.Processor @The new insert. `nil` if it could not be created.
function Gall.Route:addPluginInsert(pluginName, pluginType, pluginPreset, index, activate)

  pluginPreset = pluginPreset or ""  --cannot be nil
  index = index or -1
  if activate == nil then activate = true end

  local proc = ARDOUR.LuaAPI.new_plugin(Session, pluginName, pluginType, pluginPreset)

  local resultCode = self.ao:add_processor_by_index(proc, index, nil, activate)

  if resultCode == 0 then
    --success
    return Gall.Processors:get(proc, self)
  end
end

---[Mixbusses only] Gets the number of this Mixbus.
---@return integer @The number. 1-based; the first Mixbus is 1 not 0. -1 if this route is not a Mixbus.
function Gall.Route:mbGetNumber()

  --As this will never change we only need to figure it out once for this object. This would help with speed of
  --this potentially heavily used method.
  if not self.internalMbNumber then
    local _, number = Gall.Routes:getMixbusForName(self.ao:name())
    self.internalMbNumber = number
  end
  return self.internalMbNumber
end


---[Mixbusses only] Gets the internal 0-based number of this Mixbus. Same as `mbGetNumber() - 1`. I.e. 0 is the first Mixbus.
---
---Useful when calling Ardour methods as they are 0-based. e.g. `Session:get_mixbus()`.
---@return integer
function Gall.Route:mbGetIndex()
  return self:mbGetNumber() - 1
end

---[Mixbusses only] Gets all Mixbus Sends potentially sending to this Mixbus.
---@param activeOnly boolean @If true then only active sends are included
---@return Gall.List.MbSend @The sends. Empty list if none found.
function Gall.Route:mbGetSendsToThis(activeOnly)

  local ls = Gall.List.MbSend:new()

  if not self:isMb() then return ls end

  local thisNumber = self:mbGetNumber()

  --get all tracks, busses and MBs
  local rs = Gall.Routes:getAll(true, false, true, true, true, true)

  for r in rs:iter() do
    for mbs in r:getMbSends(activeOnly):iter() do
      if mbs:getNumber() == thisNumber then
        ls:add(mbs)
      end
    end
  end

  return ls
end

---[Mixbusses only] Gets the value of the Tape Drive (Saturation) control of this Mixbus.
---@return number @The value. `nil` if this is not a Mixbus.
function Gall.Route:mbGetTapeDriveValue()
  local pi = self:getPluginInsertsForName("Input Stage"):getSoleObject()
  if pi then
    --getPluginParameterValue() also returns the index; no need to return that here
    local v = pi:getPluginParameterValue("Tape Drive")
    return v
  end
end

---[Mixbusses only] Sets the value of the Tape Drive (Saturation) control of this Mixbus.
---@param value number @Range = -30.0 (knob full left) .. 10.0 (knob full right). -10.0 is the default position (knob centre). Values outside of this range have no effect; no change.
function Gall.Route:mbSetTapeDriveValue(value)
  local pi = self:getPluginInsertsForName("Input Stage"):getSoleObject()
  if pi then pi:setPluginParameterValue("Tape Drive", value) end
end

---[Mixbusses only] Gets the value of the Stereo Width control of this Mixbus.
---@return number @The value. `nil` if this is not a Mixbus.
function Gall.Route:mbGetStereoWidthValue()
  local pi = self:getPluginInsertsForName("Input Stage"):getSoleObject()
  if pi then
    --getPluginParameterValue() also returns the index; no need to return that here
    local v = pi:getPluginParameterValue("Stereo Width")
    return v
  end
end

-- value: 0.0 (mono) .. 1.0 (full stereo)

---[Mixbusses only] Sets the value of the Stereo Width control of this Mixbus.
---@param value number @Range = 0.0 (mono) .. 1.0 (full stereo)
function Gall.Route:mbSetStereoWidthValue(value)
  local pi = self:getPluginInsertsForName("Input Stage"):getSoleObject()
  if pi then pi:setPluginParameterValue("Stereo Width", value) end
end


-- *****************************************************************************
-- Gall.Routes
-- *****************************************************************************

---Various functions to retrieve Routes.
---@class Gall.Routes
Gall.Routes = {}

---Gets a route.
---@param obj ARDOUR.Route | Gall.Route | string @The route to find, by object or name
---@return Gall.Route @The route. `nil` if not found. `obj` if it is a Gall.Route.
function Gall.Routes:get(obj)
  return Gall.Route:newInstance(obj)
end

---Gets the Master route.
---@return Gall.Route
function Gall.Routes:getMaster()
  return Gall.Routes:get(Session:master_out())
end

---Gets the Monitor route.
---@return Gall.Route
function Gall.Routes:getMonitor()
  return Gall.Routes:get(Session:monitor_out())
end

---Gets all routes.
---
---E.g. To get just normal routes + Mixbusses, unsorted: `getAll(false,false,true,true,true,true)`
---@param sortByPos boolean @Sort by route position. Optional; default = false.
---@param sortDesc boolean @If sortByPos = true then sorts in descending order. Optional; default = false.
---@param includeSpecial boolean @If true then includes all special routes: Mixbus, Master, Monitor and Foldbacks, unless excluded by the relevant `exclude` parameter. Optional; default = false.
---@param excludeMonitor boolean @Optional; default = false. Ignored if includeSpecial = false.
---@param excludeMaster boolean @Optional; default = false. Ignored if includeSpecial = false.
---@param excludeFoldbacks boolean @Optional; default = false. Ignored if includeSpecial = false.
---@param excludeMbs boolean @Optional; default = false. Ignored if includeSpecial = false.
---@return Gall.List.Route @The routes. Empty list if none found.
function Gall.Routes:getAll(sortByPos, sortDesc, includeSpecial, excludeMonitor, excludeMaster, excludeFoldbacks, excludeMbs)
  local lsAll = Gall.List.Route:new(Session:get_routes())
  local ls = Gall.List.Route:new()
  for r in lsAll:iter() do
    local exclude = false
    if r:isSpecial() then
      if includeSpecial then
        exclude = (excludeMonitor and r.ao:is_monitor()) or (excludeMaster and r.ao:is_master()) or (excludeMbs and r:isMb())
      else
        exclude = true
      end
    end
    if not exclude then ls:add(r) end
  end
  if sortByPos then
    ls = ls:sort(Gall.Route.getPosition, sortDesc)
  end
  return ls
end

--- Gets the route at this position.  
--- This is actual position as defined by presentation order (which includes hidden routes),
--- not necessarily what `Session:get_remote_nth_route()` gives (which does not).  
--- Monitor is ignored as it's presentation order is 0, as is the first route. `getByPosition(0)` returns the first route, never Monitor.
---comment
---@param position number @The position. 0-based; the first route is 0 not 1.
---@return Gall.Route @The route. `nil` if not found.
function Gall.Routes:getByPosition(position)
  local rs = Gall.Routes:getAll(false, false, true, true)  -- Monitor is 0 as is the first route, so exclude Monitor
  for r in rs:iter() do
    if r:getPosition() == position then return r end
  end
end

--- Gets the selected routes. Includes Master and Mixbusses, not VCAs (which are not routes) or Monitor (which cannot be selected).  
---@param sortByPos boolean @Sort by route position. Optional; default = false.
---@param sortDesc boolean @If sortByPos = true then sorts in descending order. Optional; default = false.
---@return Gall.List.Route @The routes. Empty list if none found.
function Gall.Routes:getSelected(sortByPos, sortDesc)
  local ls = Gall.List.Route:new(Editor:get_selection().tracks:routelist())
  if sortByPos then ls:sort(Gall.Route.getPosition, sortDesc) end
  return ls
end

---Gets all Mixbusses.
---@return Gall.List.Route @The routes. Empty list if there are none.
function Gall.Routes:getMixbusAll()
  return Gall.Routes:getMixbus1to8():addList(Gall.Routes:getMixbus9to12())
end

---Gets Mixbusses 1 to 8.
---@return Gall.List.Route @The routes. Empty list if there are none.
function Gall.Routes:getMixbus1to8()
  local ls = Gall.List.Route:new()
  if Gall.App:isMixbus() or Gall.App:is32c() then
    for i = 1, 8 do
      ls:add(Gall.Routes:get(Session:get_mixbus(i - 1)))
    end
  end
  return ls
end

---Gets Mixbusses 9 to 12.
---@return Gall.List.Route @The routes. Empty list if there are none.
function Gall.Routes:getMixbus9to12()
  local ls = Gall.List.Route:new()
  if Gall.App:is32c() then
    for i = 9, 12 do
      ls:add(Gall.Routes:get(Session:get_mixbus(i - 1)))
    end
  end
  return ls
end

---Gets Mixbus with this number.
---@param number number @1-based. The first Mixbus is number 1 not 0.
---@return Gall.Route
function Gall.Routes:getMixbusForNumber(number)
  return Gall.Routes:get(Session:get_mixbus(number - 1))
end

---Gets the Mixbus of this name.
---@param name string
---@return Gall.Route @The route. `nil` if not found.
---@return integer @The number of the Mixbus. -1 if not found.
function Gall.Routes:getMixbusForName(name)
  local ls = Gall.Routes:getMixbusAll()
  for i = 1, #ls do
    if ls[i].ao:name() == name then return ls[i], i end
  end
  return nil, -1
end


-- *****************************************************************************
-- Gall.Vca
-- *****************************************************************************

---Wraps an ARDOUR.VCA.
---@class Gall.Vca : Gall.ArdourObject
---@field ao ARDOUR.VCA
Gall.Vca = Gall.ArdourObject:newSubclass("Gall.Vca")

function Gall.Vca:findAoByName(name)
  local r = Session:vca_manager():vca_by_name(name)
  if not r or r:isnil() then return nil end
  return r
end

function Gall.Vca:isCorrectAoType(ardourObject)
  return ardourObject:to_vca()
end


-- *****************************************************************************
-- Gall.Vcas
-- *****************************************************************************

---Various functions to retrieve VCAs.
---@class Gall.Vcas
Gall.Vcas = {}

---Gets a VCA.
---@param obj ARDOUR.VCA | string | Gall.Vca @The VCA to find, by object or name
---@return Gall.Vca @The VCA. `nil` if not found. `obj` if it is a Gall.Vca.
function Gall.Vcas:get(obj)
  return Gall.Vca:newInstance(obj)
end

---Gets all VCAs.
---@return Gall.List.Vca @The VCAs. Empty list if none found.
function Gall.Vcas:getAll()
  return Gall.List.Vca:new(Session:vca_manager():vcas())
end


-- *****************************************************************************
-- Gall.Port
-- *****************************************************************************

---Wraps an ARDOUR.Port
---@class Gall.Port : Gall.ArdourObject
---@field ao ARDOUR.Port
Gall.Port = Gall.ArdourObject:newSubclass("Gall.Port")

function Gall.Port:findAoByName(name)
  return Session:engine():get_port_by_name(name)
end

-- isCorrectAoType(): no cast available

---Gets the ports this output port is directly connected to.
---
---If this port is an input port then a empty list is returned. Use `getConnectedFrom()` instead.
---@return Gall.List.Port @The ports. Empty list if none found.
function Gall.Port:getConnectedTo()
  local ls = Gall.List.Port:new()
  if self.ao:receives_input() then
    -- is an input port; is only connected FROM not TO; return empty list
  else
    for p in Gall.Ports:getAll("in"):iter() do
      if self.ao:connected_to(p.ao:name()) then
        ls:add(p)
      end
    end
  end
  return ls
end

---Gets the ports this input port is directly connected from.
---
---If this port is an output port then a empty list is returned. Use `getConnectedTo()` instead.
---@return Gall.List.Port @The ports. Empty list if none found.
function Gall.Port:getConnectedFrom()
  local ls = Gall.List.Port:new()
  if self.ao:sends_output() then
    -- is an output port; is only connected TO not FROM; return empty list
  else
    local selfAoName = self.ao:name()
    for p in Gall.Ports:getAll("out"):iter() do
      if p.ao:connected_to(selfAoName) then
        ls:add(p)
      end
    end
  end
  return ls
end

---Is an input port.
---@return boolean
function Gall.Port:isInput()
  return self.ao:receives_input()
end

---Is a MIDI port. False means that is an audio port.
---@return boolean
function Gall.Port:isMidi()
  return self.ao:to_audioport():isnil()
end

---Gets the direction of the port: input or output.
---@return string @'"in" | "out"'
function Gall.Port:getInOutType()
  return Gall.Util:iif(self:isInput(), "in", "out")
end

---Gets the data type of the port: MIDI or audio.
---@return string @'"midi" | "audio"'
function Gall.Port:getDataType()
  return Gall.Util:iif(self:isMidi(), "midi", "audio")
end


-- *****************************************************************************
-- Gall.Ports
-- *****************************************************************************

---Various functions to retrieve Ports.
Gall.Ports = {}

---Gets a port.
---@param obj ARDOUR.Port | Gall.Port | string @The port to find, by object or name
---@return Gall.Port @The port. `nil` if not found. `obj` if it is a Gall.Port.
function Gall.Ports:get(obj)
  return Gall.Port:newInstance(obj)
end

---Gets all used ports.
---@param inOut string | nil @Direction type: "in" for input ports only, "out" for output ports only, `nil` for both
---@param audioMidi string | nil @Data type: "midi" for MIDI ports only, "audio" for audio ports only, `nil` for both
---@return Gall.List.Port @The ports. Empty list if none found.
function Gall.Ports:getAll(inOut, audioMidi)
  local ls = Gall.List.Port:new()
  if not audioMidi or audioMidi == "midi" then
    local _, t = Session:engine():get_ports(ARDOUR.DataType("midi"), ARDOUR.PortList()) -- table t holds argument references. t[2] is the PortList.
    for e in t[2]:iter() do
      if not inOut or (inOut == "in" and e:receives_input()) or (inOut == "out" and e:sends_output()) then
        ls:add(Gall.Ports:get(e))
      end
    end
  end
  if not audioMidi or audioMidi == "audio" then
    local _, t = Session:engine():get_ports(ARDOUR.DataType("audio"), ARDOUR.PortList()) -- table t holds argument references. t[2] is the PortList.
    for e in t[2]:iter() do
      if not inOut or (inOut == "in" and e:receives_input()) or (inOut == "out" and e:sends_output()) then
        ls:add(Gall.Ports:get(e))
      end
    end
  end
  return ls
end


-- *****************************************************************************
-- Gall.Group
-- *****************************************************************************

---Wraps an ARDOUR.RouteGroup.
---@class Gall.Group : Gall.ArdourObject
---@field ao ARDOUR.RouteGroup
Gall.Group = Gall.ArdourObject:newSubclass("Gall.Group")

function Gall.Group:findAoByName(name)
  for g in Session:route_groups():iter() do
    if g:name() == name then return g end
  end
end

-- isCorrectAoType(): no cast available for groups

---Gets all routes in this group.
---@param excludeMbs boolean @True = exclude Mixbusses
---@return Gall.List.Route @The routes. Empty list if none found.
function Gall.Group:getRoutes(excludeMbs)
  local ls = Gall.List.Route:new()
  local rs = Gall.List.Route:new(self.ao:route_list())
  for r in rs:iter() do
    if not (excludeMbs and r:isMb()) then ls:add(r) end
  end
  return ls
end

---Gets all non-hidden routes in this group.
---@param excludeMbs boolean @True = exclude Mixbusses
---@return Gall.List.Route @The routes. Empty list if none found.
function Gall.Group:getVisibleRoutes(excludeMbs)
  local ls = Gall.List.Route:new()
  for r in self:getRoutes(excludeMbs):iter() do
    if not r.ao:is_hidden() then ls:add(r) end
  end
  return ls
end

---Hide/unhide this group.
---
-- NOTE: To prevent this from deactivating groups when hiding them, set `hiding-groups-deactivates-groups = 0`
-- in "config" file. There doesn't seem to be a way to do this in Ardour's Preferences UI.
-- This was found in set_hidden() in source code's route_group.cc.
---@param hide boolean @If true then hide, else unhide
function Gall.Group:setHidden(hide)
  self.ao:set_hidden(hide, nil) -- 2nd arg = "only if selected"
end

---Hides this group if all of its routes are hidden.
function Gall.Group:setHiddenIfNoVisibleRoutes()
  self:setHidden(self:getVisibleRoutes(true):size() == 0)
end


-- *****************************************************************************
-- Gall.Groups
-- *****************************************************************************

---Various functions to retrieve Groups, also for creating a new Group in Ardour.
Gall.Groups = {}

---Gets a group.
---@param obj ARDOUR.RouteGroup | string | Gall.Group @The group to find, by object or name
---@return Gall.Group @The group. `nil` if not found. `obj` if it is a Gall.Group.
function Gall.Groups:get(obj)
  return Gall.Group:newInstance(obj)
end

---Gets the groups of all the given routes.
---@param routes Gall.List.Route @The routes
---@return Gall.List.Group @The groups. Empty list if none found.
function Gall.Groups:getForRoutes(routes)
  local ls = Gall.List.Group:new()
  for r in routes:iter() do
    local g = r:getGroup()
    if g then ls:addUnique(g) end
  end
  return ls
end

---Gets the groups of all the selected routes.
---@return Gall.List.Group
function Gall.Groups:getSelected()
  return self:getForRoutes(Gall.Routes:getSelected())
end

---Creates a new group in Ardour.
---
---If a group already exists with the given name then the name will be suffixed with 1,2,3,... until a unique name is found.
---@param name string @The name of the new group
---@return Gall.Group @The new group
function Gall.Groups:create(name)
  local newName = name
  if Gall.Groups:get(newName) then
    --already exists
    local i = 1
    repeat
      newName = name.." "..i
      i = i + 1
    until not Gall.Groups:get(newName)
  end
  local g = Session:new_route_group(newName)
  return Gall.Groups:get(g)
end


-- *****************************************************************************
-- Gall.InternalSend
-- *****************************************************************************

---Wraps an ARDOUR.InternalSend
---@class Gall.InternalSend : Gall.ArdourObject
---@field ao ARDOUR.InternalSend
Gall.InternalSend = Gall.ArdourObject:newSubclass("Gall.InternalSend")

-- findAoByName(): they do have names but not unique so can't be found

function Gall.InternalSend:isCorrectAoType(ardourObject)
  return ardourObject:to_internalsend()
end

---Gets this send as a Gall.Processor.
---@return Gall.Processor
function Gall.InternalSend:toProcessor()
  return Gall.Processors:get(self.ao, self:getSource())
end

---Gets the source route: the route that this send belongs to.
---@return Gall.Route
function Gall.InternalSend:getSource()
  return Gall.Routes:get(self.ao:source_route())
end

---Gets the target route: the route this send is sending to.
---@return Gall.Route
function Gall.InternalSend:getTarget()
  return Gall.Routes:get(self.ao:target_route())
end

---Sets the gain of this send.  
--- - 0 = no gain (nothing sent)  
--- - 1 = nominal 0dB mark  
--- - 2 = +6dB (max)  
---Other examples:  
--- - 0.5 = -6dB  
--- - 0.25 = -12dB  
--- - 0.125 = -18dB  
--- - 1.5 = +3.5dB
---@param gain number
function Gall.InternalSend:setGain(gain)
  self.ao:gain_control():set_value(gain, PBD.GroupControlDisposition.NoGroup)
end

---Activates/deactivates this send.
---@param active boolean @Activates if true, else deactivates
function Gall.InternalSend:setActive(active)
  if active then self.ao:activate() else self.ao:deactivate() end
end


-- *****************************************************************************
-- Gall.InternalSends
-- *****************************************************************************

---Various functions to retrieve InternalSends.
Gall.InternalSends = {}

---Gets an internal send.
---@param obj ARDOUR.InternalSend | Gall.InternalSend @The send to find, by object
---@return Gall.InternalSend @The send. `nil` if not found. `obj` if it is a Gall.InternalSend.
function Gall.InternalSends:get(obj)
  return Gall.InternalSend:newInstance(obj)
end


-- *****************************************************************************
-- Gall.MbSend
-- *****************************************************************************

---Represents a Mixbus Send. It's `ao` is an ARDOUR.AutomationControl that is the send's "Active" control.
---@class Gall.MbSend : Gall.ArdourObject
---@field ao ARDOUR.AutomationControl
---@field sourceRoute Gall.Route @The from-route; the one that contains/owns the send
---@field mbNumber integer @The number of the Mixbus this is sending to. 1-based; the first Mixbus is 1 not 0.
Gall.MbSend = Gall.ArdourObject:newSubclass("Gall.MbSend")

Gall.MbSend.mbNumber = nil
Gall.MbSend.sourceRoute = nil

-- findAoByName(): they do have names but not unique so can't be found
-- isCorrectAoType(): there's no cast check unique to ARDOUR.AutomationControl

---Gets the source route: the route that this send belongs to.
---@return Gall.Route
function Gall.MbSend:getSource()
  return self.sourceRoute
end

---Gets the target Mixbus: the route this send is sending to.
---@return Gall.Route
function Gall.MbSend:getTarget()
  return Gall.Routes:get(Session:get_mixbus(self.mbNumber - 1))
end

---Gets the number of the Mixbus being sent to. 1-based; the first Mixbus is 1 not 0.
---@return integer
function Gall.MbSend:getNumber()
  return self.mbNumber
end

---Gets the internal 0-based number of this send/Mixbus.
---
---For sends 9..12 on Mixbusses 1..8 this will give 0..3. For other routes it is the same as `getNumber() - 1`.
--
---Useful when calling Ardour methods as they are 0-based. e.g. `ARDOUR.Route:send_level_controllable()`.
---@return integer
function Gall.MbSend:getIndex()
  if self.sourceRoute:isMb1to8() then
    return self.mbNumber - 9
  end
  return self.mbNumber - 1
end

---Gets the name of the send, which is the name of the Mixbus being sent to.
---@return string
function Gall.MbSend:getName()
  return Session:get_mixbus(self.mbNumber - 1):name()
end

---Gets whether this send is active (on).
---@return boolean
function Gall.MbSend:isActive()
  return self.ao:get_value() == 1
end

---Activates/deactivates this send. Does not affect sends in other routes of this route's Group.
---@param active boolean @True = activate, else deactivate
function Gall.MbSend:setActive(active)
  self.ao:set_value(Gall.Util:iif(active, 1, 0), PBD.GroupControlDisposition.NoGroup)
end

---Gets the send gain.
---@return number
function Gall.MbSend:getGain()
  return self.sourceRoute.ao:send_level_controllable(self:getIndex()):get_value()
end

---Sets the send gain. This also activates the send (only this; not sends in other routes of this route's Group).  
--- - 1 = 0dB (nominal, default setting for the control)  
--- - 0 = -75dB (min) - actually ~0.00018  
--- - 5.62 = +15dB (max) - actually ~5.623413  
---Other examples:  
--- - 0.5 = -6dB  
--- - 0.25 = -12dB  
--- - 0.125 = -18dB  
--- - 2.0 = +6dB  
--- - 4.0 = +12dB
---@param value number @Gain level. See above. Values are automatically limited to the 0..5.62 range; e.g. anything above 5.62 is treated as 5.62.
function Gall.MbSend:setGain(value)
  self.sourceRoute.ao:send_level_controllable(self:getIndex()):set_value(value, PBD.GroupControlDisposition.NoGroup)
end

---Resets this Mixbus Send. Sets gain and panning back to defaults and deactives.
function Gall.MbSend:reset()
  local i = self:getIndex()
  --do the "enable" ones last because setting the others will activate them
  self.sourceRoute.ao:send_level_controllable(i):set_value(1, PBD.GroupControlDisposition.NoGroup)
  if Gall.App:is32c() then
    self.sourceRoute.ao:send_pan_azimuth_controllable(i):set_value(0.5, PBD.GroupControlDisposition.NoGroup)  
    self.sourceRoute.ao:send_pan_azimuth_enable_controllable(i):set_value(0, PBD.GroupControlDisposition.NoGroup)
  end
  self.sourceRoute.ao:send_enable_controllable(i):set_value(0, PBD.GroupControlDisposition.NoGroup)
end


-- *****************************************************************************
-- Gall.MbSends
-- *****************************************************************************

---Various functions to retrieve Mixbus Sends.
---@class Gall.MbSends
Gall.MbSends = {}

---Gets a Mixbus Send.
---@param sourceRoute ARDOUR.Route | string | Gall.Route @The route owning the send, as an object or name
---@param mbNumber integer @The number of the Mixbus this is sending to. 1-based; the first Mixbus is 1 not 0.
---@return Gall.MbSend @The send. `nil` if no such route or send.
function Gall.MbSends:get(sourceRoute, mbNumber)

  local r = Gall.Routes:get(sourceRoute)
  if r then
    if Gall.App:isArdour() or r:isMaster() or r:isMonitor() or r:isMb9to12() or r:isFoldback() then return nil end

    local ao = nil
    if r:isMb1to8() then
      if Gall.App:is32c() then
        --on MBs, send 9 = send_enable_controllable(0)
        ao = r.ao:send_enable_controllable(mbNumber - 9)
      end
    else
      --on other routes, send 1 = send_enable_controllable(0)
      ao = r.ao:send_enable_controllable(mbNumber - 1)
    end
    if ao and not ao:isnil() then
      local obj = Gall.MbSend:newInstance(ao)
      obj.mbNumber = mbNumber
      obj.sourceRoute = r
      return obj
    end
  end
end

-- *****************************************************************************
-- Gall.Processor
-- *****************************************************************************

---Wraps an ARDOUR.Processor.
---@class Gall.Processor : Gall.ArdourObject
---@field ao ARDOUR.Processor
---@field route Gall.Route
Gall.Processor = Gall.ArdourObject:newSubclass("Gall.Processor")

Gall.Processor.route = nil

-- findAoByName(): they do have names but not unique so can't be found
-- isCorrectAoType(): there are many casts for processor; for simplicity just assume that it is correct

---Get sthe route this processor belongs to.
---@return Gall.Route
function Gall.Processor:getRoute()
  return self.route
end

---Gets the ID, as returned by `.ao:to_stateful():id():to_s()`.
---@return string
function Gall.Processor:getId()
  return self.ao:to_stateful():id():to_s()
end

---Returns true if this is a PluginInsert processor.
---@return boolean
function Gall.Processor:isPluginInsert()
  return not self.ao:to_plugininsert():isnil()
end

---Convenience method for casting to an ARDOUR.PluginInsert.
---
---Shorthand for `.ao:to_plugininsert()` but returns `nil` if this is not a PluginInsert.
---@return ARDOUR.PluginInsert
function Gall.Processor:pi()
  local pi = self.ao:to_plugininsert()
  if not pi:isnil() then return pi end
end

---Gets the Plugin of this PluginInsert.
---@return ARDOUR.Plugin @The plugin. `nil` if this is not a PluginInsert.
function Gall.Processor:getPlugin()
  if self.ao:to_plugininsert():isnil() then return nil end
  return self.ao:to_plugininsert():plugin(0)
end

---Returns a Lua table with the plugin's control parameters: labels (as keys) and values (as values).
---@return table @The parameters. `nil` if this is not a PluginInsert.
function Gall.Processor:getPluginParameters()

  if self.ao:to_plugininsert():isnil() then return nil end

  local t = {}
  local p = self:getPlugin()

  for i = 0, p:parameter_count()-1 do
    if p:parameter_is_control(i) then
      --store_recall_mixer.lua also does this: if plug:parameter_is_input (j) and label ~= "hidden" and label:sub (1,1) ~= "#" then
      local label = p:parameter_label(i)
      local val = ARDOUR.LuaAPI.get_processor_param(self.ao, i, true)
      t[label] = val
    end
  end
  return t
end

---Gets the value of a given control parameter of the plugin, and its control index, and its parameter index.
---@param name string @Name of the parameter to retrieve
---@return table @The value, as returned by `ARDOUR.LuaAPI.get_processor_param()`. `nil` if this is not a PluginInsert.
---@return number @The control index, as provided to `ARDOUR.LuaAPI.get_processor_param()`. `nil` if this is not a PluginInsert.
---@return number @The parameter index. `nil` if this is not a PluginInsert.
function Gall.Processor:getPluginParameterValue(name)

  if self.ao:to_plugininsert():isnil() then return nil, -1 end

  local p = self:getPlugin()
  local ctrlIdx = 0
  for i = 0, p:parameter_count()-1 do
    if p:parameter_is_control(i) then
      if p:parameter_label(i) == name then
         return ARDOUR.LuaAPI.get_processor_param(self.ao, ctrlIdx, true), ctrlIdx, i
      end
      ctrlIdx = ctrlIdx + 1
    end
  end
end

---Sets the value of a given control parameter of the plugin. Does nothing if this is not a PluginInsert.
---@param name string @Name of the parameter to set
---@param value number @The value to set it to
function Gall.Processor:setPluginParameterValue(name, value)
  if self.ao:to_plugininsert():isnil() then return end
  local _, ctrlIdx = self:getPluginParameterValue(name)
  if ctrlIdx ~= nil then
    ARDOUR.LuaAPI.set_processor_param(self.ao, ctrlIdx, value)
  end
end

---Gets the automation control of a given control parameter of the plugin.
---@param name string @Name of the parameter to retrieve
---@return ARDOUR.AutomationControl @The control. `nil` if not found or this is not a PluginInsert.
function Gall.Processor:getPluginAutomationControl(name)
  if self.ao:to_plugininsert():isnil() then return end
  local _,_,pidx = self:getPluginParameterValue(name)
  if pidx then
    return self:pi():to_automatable():automation_control(Evoral.Parameter(ARDOUR.AutomationType.PluginAutomation, 0, pidx), false)
  end
end

---Gets all sidechain (and external) sends feeding this processor's sidechain. That is, "connected to" - 
---it does not care if those sends have 0 gain or are inactive.
---@param ports Gall.List.Port @Optional. If present then it only includes sends feeding these specific sidechain ports. If `nil` then it includes sends feeding any of the sidechain ports.
---@return Gall.List.Processor @The sends. Empty list if none found or this processor has no sidechain.
function Gall.Processor:getSidechainSendsToThis(ports)

  local ls = Gall.List.Processor:new()

  local sci = self.ao:to_plugininsert():sidechain_input()
  if sci:isnil() then return ls end

  local t = {}  --names of ports on this sidechain

  if ports then
    for port in ports:iter() do
      table.insert(t, port.ao:name())
    end
  else
    for i = 0, sci:n_ports():n_total()-1 do
      table.insert(t, sci:nth(i):name())
    end
  end

  for r in Gall.Routes:getAll(false, false, true):iter() do
    for p in r:getProcessors():iter() do
      local yesThisProcessor = false
      -- Only interested in SC sends and external sends*. Those can cast to to_send() but not to_internal_send().
      -- * External sends are included only because I could not find a way to differentiate between 
      -- those and SC sends. to_sidechain() sounds like the way to do it but gives isnil() for SC sends.
      if not p.ao:to_send():isnil() and p.ao:to_internalsend():isnil() then
        local out = p.ao:to_send():output()
        for i = 0, out:n_ports():n_total()-1 do
          local outPort = out:nth(i)
          for _,n in pairs(t) do
            if outPort:connected_to(n) then
              ls:addUnique(p)
              yesThisProcessor = true
              break
            end
          end
          if yesThisProcessor then
            break -- no need to keep looking at this proc's ports
          end
        end
        if yesThisProcessor then
          break -- no need to keep looking at this proc
        end
     end
   end
  end

  return ls
end

---Gets all routes whose outputs are feeding this processor's sidechain. That is, "connected to" - 
---it does not care if those routes have 0 gain or are inactive or muted.
---@param ports Gall.List.Port @Optional. If present then it only includes routes feeding these specific sidechain ports. If `nil` then it includes routes feeding any of the sidechain ports.
---@return Gall.List.Route @The routes. Empty list if none found or this processor has no sidechain.
function Gall.Processor:getSidechainRoutesToThis(ports)

  local ls = Gall.List.Route:new()

  local sci = self.ao:to_plugininsert():sidechain_input()
  if sci:isnil() then return ls end

  local t = {}  --names of ports on this sidechain

  if ports then
    for port in ports:iter() do
      table.insert(t, port.ao:name())
    end
  else
    for i = 0, sci:n_ports():n_total()-1 do
      table.insert(t, sci:nth(i):name())
    end
  end

  for r in Gall.Routes:getAll(false, false, true):iter() do
    local yesThisRoute = false
    local out = r.ao:output()
    for i = 0, out:n_ports():n_total()-1 do
      local outPort = out:nth(i)
      for _,n in pairs(t) do
        if outPort:connected_to(n) then
          ls:add(r)
          yesThisRoute = true
          break
        end
      end
      if yesThisRoute then
        break -- no need to keep looking at this route's ports
      end
    end
  end

  return ls
end

---Creates an audio sidechain send on the given route, feeding this processor. This processor must be a PluginInsert.
---
---It creates a sidechain port if one does not already exist, and maps it to the sidechain pin. If the plugin has two
---(or more) sidechain pins then a second port is also created (if does not already exist) and mapped to the 2nd pin.
---Sidechain pins 3 and above are ignored - this method only supports mono and stereo sidechains. Also, if the pin(s)
---are already mapped to port(s) then they remain as they are, not remapped by this method.
---
---If the sidechain is mono (single pin) then:
--- - Channels 1 & (if present) 2 of the send are connected to sidechain port 1.
---
---If the sidechain is stereo (2 or more pins) then:
--- - If the send is mono then it is connected to sidechain ports 1 & 2.
--- - If the send is not mono then its channel 1 is connected to sidechain port 1, and its channel 2 is connected to sidechain port 2.
---
---NOTE: The displayed name of the sidechain source in the sidechain UI (processor's Pin Connections) might 
---be this processor's name, "...", or possibly something wrong. After a session save-reload this often changes to be 
---the name of the send instead, e.g. "send 1". Also, the send name sometimes ends up being "send {number}" instead of
---the normal naming convention: ">SC {plugin}".
---@param route Gall.Route | ARDOUR.Route | string @The route to create the send in, by object or name
---@param postFade boolean @If true then creates a post-fade send, else a pre-fade send
---@return Gall.Processor @The created sidechain send. `nil` if could not be created, e.g. because this is not a PluginInsert or the route could not be found
---@return string @The reason it could not be created, if first return value is `nil`
function Gall.Processor:createSidechainSend(route, postFade)

  --[[
    Further info on that note on source name...

    When a new SC is created manually via the UI, the name displayed is the name of the send's route and the 
    number of the send, e.g. "Bass (3)" which means "send 3" which is on route "Bass".
    It does that by setting the "pretty name" of the send's IO output.
    There is no Lua method for setting that name, thus the name ends up being whatever Ardour decides, which
    is seemingly randomly one of the three options described above.
  --]]

  if not self:isPluginInsert() then return nil, "This is not a PluginInsert." end

  --route of the send
  local gr = Gall.Routes:get(route)
  if gr == nil then return nil, "No such route." end
  local r = gr.ao

  local scPinIdxs = self:getSidechainAudioPinIndexes()
  if #scPinIdxs == 0 then return nil, "No sidechain pins found." end

  local sci = self:pi():sidechain_input()
  if not self:pi():has_sidechain() then
    --No SC; turn it on. This also creates a port for the SC.
    self:getRoute().ao:add_sidechain(self.ao)
    sci = self:pi():sidechain_input()
    if sci:isnil() then return nil, "sidechain_input():isnil() == true after add_sidechain()." end
  end

  --There could be any number of SC pins. We're only supporting mono and stereo here. 
  --If there are multiple pins then treat as "stereo" (pins 3 onwards will be ignored).
  local scIsStereo = #scPinIdxs >= 2 --

  --if there are 2 (or more) audio SC pins then ensure that that there are at least 2 audio SC ports
  if scIsStereo and sci:n_ports():n_audio() < 2 then
    local numPortsToAdd = 2 - sci:n_ports():n_audio()
    for i = 1, numPortsToAdd do
      sci:add_port("", nil, ARDOUR.DataType("audio"))
    end
  end

  local scPortIdxs = self:getSidechainAudioPortIndexes()

  --connect the pins
  --get the current input mapping and append SC mapping
  local cm = self:pi():input_map(0)
  --the first SC port
  cm:set(ARDOUR.DataType("audio"), scPinIdxs[1], scPortIdxs[1])
  --the second SC port (if we have one)
  if scIsStereo then
    cm:set(ARDOUR.DataType("audio"), scPinIdxs[2], scPortIdxs[2])
  end
  self:pi():set_input_map(0, cm)

  --create the send
  local sendBeforeProc = r:amp()
  if postFade then
    sendBeforeProc = ARDOUR.LuaAPI.nil_proc()
  end
  local s = ARDOUR.LuaAPI.new_send(Session, r, sendBeforeProc)
  if s:isnil() then return nil, "send:isnil() == true after new_send()." end
  -- mark as sidechain send
  s:to_send():set_remove_on_disconnect(true)
  local so = s:to_send():output()
  local sendIsStereo = so:n_ports():n_audio() >= 2  -- again, using "stereo" to mean multichannel and we're ignoring any beyond the 2nd

  --connect the send to the sidechain port(s)
  local targetPort1 = sci:audio(0)
  local targetPort2 = sci:audio(0)
  if scIsStereo then
    targetPort2 = sci:audio(1)
  end
  so:audio(0):connect(targetPort1:name())
  -- Don't connect to the second SC port yet (if any). It results in the send being named "> send X" instead 
  -- of "> SC {processor name}". Instead, connect after the creation of the 'dummy' send below - that seems to avoid the problem.

  --Create another SC send and remove it.
  --This is a hack to make the above one display as a SC send instead of an external send. Don't know why. Discovered by accident.
  local s2 = ARDOUR.LuaAPI.new_send(Session, r, sendBeforeProc)
  if not s2:isnil() then gr:removeProcessor(s2) end

  --2nd connection (see above)
  if scIsStereo then
    if sendIsStereo then
      so:audio(1):connect(targetPort2:name())
    else
      so:audio(0):connect(targetPort2:name())
    end
  else
    if sendIsStereo then
      so:audio(1):connect(targetPort1:name())
    end
  end

  return Gall.Processors:get(s, gr)
end

---Gets the numbers (indexes, 0-based) of the audio sidechain input pins on the plugin. Not the sidechain 
---*ports* on this processor; the plugin pins that those ports can be mapped to.
---
---These values can be used for port-pin mapping, i.e. in the ARDOUR.ChanMapping used by `PluginInsert:set_input_map()`.
---
---E.g. If the plugin has 2 audio input pins and 2 audio sidechain input pins then this will return
---values 2 & 3 (0 & 1 being the audio pins).
---
---WARNING: this is not 100% reliable.
---@return table @The indexes (0-based) of the sidechain pins. Empty table if none found or this is not a PluginInsert.
function Gall.Processor:getSidechainAudioPinIndexes()
  --[[
      One way to find whether a pin is a sidechain is this:
        self:getPlugin():describe_io_port(ARDOUR.DataType("Audio"), true, i).is_sidechain
      ...but it's not always true. E.g. it works for ACE Compressor but not for Calf Sidechain 
      Compressor; that has 2 SC pins, named "sidechain" (.name property), but the above 
      is_sidechain is false for both.

      So this method does not rely on that approach. It also uses natural_input[output]_streams()
      to get the audio in/out pin counts and assumes that if the in count > out count then
      those extra ins are sidechains. This is not always correct, e.g. an autopanner might have
      1 in and 2 outs, and if it also had a SC in then this logic would interpret that as being
      an audio in not a SC in. The is_sidechain check is also done to help decide but, as above,
      is not guaranteed to be correct either.
  ]]

  local t = {}
  if self:isPluginInsert() then
    local pi = self:pi()
    local p = self:getPlugin()
    local lastAudioInputPinIdx = pi:natural_input_streams():n_audio() - 1
    local lastAudioOutputPinIdx = pi:natural_output_streams():n_audio() - 1
    for i = 0, lastAudioInputPinIdx do
      if p:describe_io_port(ARDOUR.DataType("audio"), true, i).is_sidechain or i > lastAudioOutputPinIdx then
        table.insert(t, i)
      end
    end
  end
  return t
end

---Gets the numbers (indexes, 0-based) of the audio sidechain ports of this processor. Not the sidechain 
---plugin *pins*; the ports on this processor that those pins can be mapped to.
---
---These values can be used for port-pin mapping, i.e. in the ARDOUR.ChanMapping used by `PluginInsert:set_input_map()`.
---
---E.g. If the processor has 2 audio input ports and 2 audio sidechain ports then this will return
---values 2 & 3 (0 & 1 being the audio ports).
---@return table @The indexes (0-based) of the sidechain ports. Empty table if none found or this is not a PluginInsert.
function Gall.Processor:getSidechainAudioPortIndexes()

  local t = {}
  if self:isPluginInsert() and self:pi():has_sidechain() then
    local pi = self:pi()
    local sci = pi:sidechain_input()
    local audioInPortsCount = pi:input_streams():n_audio() --non-SC audio input ports
    for i = 0, sci:n_ports():n_audio() - 1 do
      table.insert(t, audioInPortsCount + i)
    end
  end
  return t
end

---Returns true if this processor is after the fader.
---@return boolean
function Gall.Processor:isPostFade()
  return self:getRoute():isProcessorPostFade(self)
end


-- *****************************************************************************
-- Gall.Processors
-- *****************************************************************************

---Various functions to retrieve Processors.
---@class Gall.Processors
Gall.Processors = {}

---Gets a processor.
---@param processor ARDOUR.Processor | Gall.Processor @The processor, as an object only
---@param route Gall.Route | ARDOUR.Route | string @The route housing this processor, by object or name
---@return Gall.Processor @The processor
function Gall.Processors:get(processor, route)
  local obj = Gall.Processor:newInstance(processor)
  obj.route = Gall.Routes:get(route)
  return obj
end


-- *****************************************************************************
-- Gall.App
-- *****************************************************************************

---Provides information about the Ardour/Mixbus application instance.
---@class Gall.App
Gall.App = {
  APP_ARDOUR = 1,
  APP_MIXBUS = 2,
  APP_32C = 3,
  appType = nil
}

-- determine which application is running
if not pcall(function() local first_check = Session:get_mixbus(0) end) then
  Gall.App.appType = Gall.App.APP_ARDOUR
elseif not Session:get_mixbus(11):isnil() then
  Gall.App.appType = Gall.App.APP_32C
else
  Gall.App.appType = Gall.App.APP_MIXBUS
end

---Gets the application type in name form.
---@return string @'"Ardour" | "Mixbus" | "32C"'
function Gall.App:getName()
  if Gall.App.appType == Gall.App.APP_ARDOUR then return "Ardour" end
  if Gall.App.appType == Gall.App.APP_MIXBUS then return "Mixbus" end
  return "32C"
end

---Is Ardour.
---@return boolean
function Gall.App:isArdour()
  return Gall.App.appType == Gall.App.APP_ARDOUR
end

---Is Mixbus (not Mixbus32C).
---@return boolean
function Gall.App:isMixbus()
  return Gall.App.appType == Gall.App.APP_MIXBUS
end

---Is Mixbus32C.
---@return boolean
function Gall.App:is32c()
  return Gall.App.appType == Gall.App.APP_32C
end


-- *****************************************************************************
-- Gall.Dlg
-- *****************************************************************************

---A helper class for Lua Dialogs.
---@class Gall.Dlg
Gall.Dlg = {}

---Shows a Message dialog. An "Info" message type (icon) with just a "Close" button.
---@param title string @The dialog's window title
---@param text string @The message to show
function Gall.Dlg:msg(title, text)
  local md = LuaDialog.Message(title, text, LuaDialog.MessageType.Info, LuaDialog.ButtonType.Close)
  md:run()
  md = nil
end

-- Shows a prompt dialog with OK and Cancel buttons (or Yes and No buttons - useYesNo is optional).
-- RETURNS: true if OK or Yes button clicked, else false (including if dialog window closed).

---Shows a Message dialog, prompting the user for an 'OK/cancel' or 'yes/no' response.
-- A "Question" message type (icon).
---@param title string @The dialog's window title
---@param text string @The message to show
---@param useYesNo boolean @If true then shows Yes/No buttons, else shows OK/Cancel buttons
---@return boolean @True if the OK (or Yes) button was clicked
function Gall.Dlg:prompt(title, text, useYesNo)
  local buttons = LuaDialog.ButtonType.OK_Cancel
  if useYesNo then
    buttons = LuaDialog.ButtonType.Yes_No
  end
  local md = LuaDialog.Message(title, text, LuaDialog.MessageType.Question, buttons)
  local rv = md:run()
  md = nil
  return rv == LuaDialog.Response.OK or rv == LuaDialog.Response.Yes
end

---Shows an "entry" (text input) dialog.
---@param title string @The dialog's window title
---@param text string @The prompt message to show (if any)
---@param defaultValue string @The value to show in the textbox by default
---@return string @The entered value. `nil` if the dialog was cancelled.
function Gall.Dlg:input(title, text, defaultValue)
  if not defaultValue then defaultValue = "" end
  local dialog_options = {
    { type = "entry", key = "text", default = defaultValue, title = text }
  }
  local od = LuaDialog.Dialog(title, dialog_options)
  local rv = od:run()
  if (rv) then
    return rv["text"]
  end
end

---Shows a Message dialog with "Cannot proceed" as the title and the given text as the body.
---@param whyNot string @The message to show
function Gall.Dlg:cannotProceed(whyNot)
  self:msg("Cannot proceed", whyNot)
end


-- *****************************************************************************
-- Gall.Util
-- *****************************************************************************

---Some utility functions.
---@class Gall.Util
Gall.Util = {}

---Returns one of two values depending on a boolean test.
---@param test boolean @The test
---@param trueValue any @The value to return if `test` is true
---@param falseValue any @The value to return if `test` is false
---@return any
function Gall.Util:iif(test, trueValue, falseValue)
  if test then return trueValue else return falseValue end
end

---A convenience wrapper for `ARDOUR.LuaAPI.usleep()`, taking the duration in milliseconds instead of microseconds.
---@param ms number @Milliseconds to sleep for
function Gall.Util:sleep(ms)
  ARDOUR.LuaAPI.usleep(ms * 1000)
end


-- *****************************************************************************
-- Gall.List
-- *****************************************************************************

---A collection class to hold multiple objects of type Gall.ArdourObject (or subclass).
---
---It is recommended to use the more specialised List classes instead, e.g. Gall.List.Route, as their annotation comments define the correct parameter and return types specific to what they contain.
---@class Gall.List
---@field elemGallType any @The type of the objects contained in the list
Gall.List = {
  elemGallType = Gall.ArdourObject
}

---Creates a new list, optionally populating it with objects.
---@param elemGallType Gall.ArdourObject @The type of object to contain. Only required if `iterable` is provided or if `addIter()` will ever be invoked. Defaults to Gall.ArdourObject but the actual type should be used instead, e.g. Gall.Route.
---@param iterable any @Optional. The objects to populate the list with. Anything that has an `iter()` function which provides objects which are, or can be used to create, a Gall.ArdourObject. E.g. A Gall.Route, an ARDOUR.Route or a route name.
---@return Gall.List
function Gall.List:newInstance(elemGallType, iterable)

  local newObj = {}

  if elemGallType ~= nil then
    newObj.elemGallType = elemGallType
  end

  setmetatable(newObj, self)
  self.__index = self

  if iterable then
    newObj:addIter(iterable)
  end

  return newObj
end

---@return integer @The number of objects in the list
function Gall.List:size()
  local c = 0
  for _ in self:iter() do c = c + 1 end
  return c
end

---@return boolean @True if the list is empty
function Gall.List:isEmpty()
  return self:size() == 0
end

---Gets the sole object from the list, if there is only one object in the list.
---@return Gall.ArdourObject @The object. `nil` if the list contains multiple or no objects.
function Gall.List:getSoleObject()
  if self:size() == 1 then return self[1] end
end

---Appends a new object to the end of the list.
---@param obj Gall.ArdourObject @The object to add
---@return Gall.List @This list (self)
function Gall.List:add(obj)
  if not obj then return end
  table.insert(self, obj)
  return self
end

---Appends a new object to the end of the list, only if it is not already present.
---@param obj Gall.ArdourObject @The object to add
---@return Gall.List @This list (self)
function Gall.List:addUnique(obj)
  if not obj then return end
  for e in self:iter() do
    if e.ao == obj.ao then return end
  end
  table.insert(self, obj)
  return self
end

---Appends to this list the objects of another list.
---
---The other list can also be a table but only if its keys are numbers 1..n with no gaps in the sequence.
---@param otherList Gall.List | table
---@return Gall.List @This list (self)
function Gall.List:addList(otherList)
  for i = 1, #otherList do
    self[#self + 1] = otherList[i]
  end
  return self
end

---Appends to this list the items retrieved from invoking `iterable:iter()`.
---@param iterable any @Anything that has an `iter()` function which provides objects which are, or can be used to create, a Gall.ArdourObject. E.g. A Gall.Route, an ARDOUR.Route or a route name.
---@return Gall.List @This list (self)
function Gall.List:addIter(iterable)
  for e in iterable:iter() do
    self[#self + 1] = self.elemGallType:newInstance(e)
  end
  return self
end

---Removes this object from the list.
---@param obj Gall.ArdourObject @The object to remove
---@return Gall.List @This list (self)
function Gall.List:remove(obj)
  local idx = self:getIndexFor(obj)
  if idx then table.remove(self, idx) end
  return self
end

---Removes from this list the objects retrieved from invoking `iterable:iter()`.
---@param iterable any @Anything that has an `iter()` function which provides objects of type Gall.ArdourObject
---@return Gall.List @This list (self)
function Gall.List:removeIter(iterable)
  if iterable then
    for e in iterable:iter() do
      self:remove(e)
    end
  end
  return self
end

---Iterator.
---@return fun(): Gall.ArdourObject
function Gall.List:iter()
  local i = 0
  local n = #self
  return function()
    i = i + 1
    if i <= n then return self[i] end
  end
end

---Returns the index of a given object in this list.
---@param obj Gall.ArdourObject @The object to find
---@return integer @The index. `nil` if not found.
function Gall.List:getIndexFor(obj)
  for i = 1, #self do
    if self[i].ao == obj.ao then return i end
  end
end

---Returns the first object in the list.
---@return Gall.ArdourObject @The object. `nil` if the list is empty.
function Gall.List:front()
  if #self > 0 then return self[1] end
end

---Returns the last object in the list.
---@return Gall.ArdourObject @The object. `nil` if the list is empty.
function Gall.List:back()
  if #self > 0 then return self[#self] end
end

---Returns true if this object is in the list.
---@param obj Gall.ArdourObject @The object to find
---@return boolean
function Gall.List:contains(obj)
  for e in self:iter() do
    if e.ao == obj.ao then return true end
  end
  return false
end

---Sorts this list via a less-than/greater-than test on the field specified by `fieldSelectFunc`.
---
---E.g. If `ls` is a list of Gall.Route objects then:
---  - `ls:sort(function(r) return r:getPosition() end)` --> sorts by `Gall.Route:getPosition()` in ascending order.
---  - `ls:sort(Gall.Route.getPosition)` does the same. NOTE: that is `.getPosition`, not `:getPosition()`.
---@param fieldSelectFunc function @Function to specify the field to sort by
---@param descending boolean @If true then sorts in descending order
---@return Gall.List @This list (self)
function Gall.List:sort(fieldSelectFunc, descending)
  local sortFunc = nil
  if descending then
    sortFunc = function (a,b) return fieldSelectFunc(a) > fieldSelectFunc(b) end
  else
    sortFunc = function (a,b) return fieldSelectFunc(a) < fieldSelectFunc(b) end
  end
  table.sort(self, sortFunc)
  return self
end


-- *****************************************************************************
-- Gall.List.Base
-- *****************************************************************************

---Internal use only - should not be used directly.
---
---The base class for specialised List classes such as Gall.List.Route.
---@class Gall.List.Base
---@field innerList Gall.List
---@field gallType string @The class name of the subclass
---@field elemGallType any @The type of the objects contained in the list
Gall.List.Base = {
  innerList = nil,
  gallType = "Gall.Base.List",
  elemGallType = nil
}

---Internal use only. Use the subclass' `new()` function instead.
---@param iterable any
---@return Gall.List.Base
function Gall.List.Base:newInstance(iterable)
  local newObj = {
    innerList = Gall.List:newInstance(self.elemGallType, iterable)
  }
  setmetatable(newObj, self)
  self.__index = self
  return newObj
end

---Internal use only.
---@param subclassGallType any
---@param elemGallType any
---@return table
function Gall.List.Base:newSubclass(subclassGallType, elemGallType)
  local obj = {
    gallType = subclassGallType,
    elemGallType = elemGallType,
  }
  setmetatable(obj, self)
  self.__index = self
  return obj
end

-- The following methods are not type-specific so can be defined here instead of in each subclass.

---@return integer @The number of objects in the list
function Gall.List.Base:size()
  return self.innerList:size()
end

---@return boolean @True if the list is empty
function Gall.List.Base:isEmpty()
  return self.innerList:isEmpty()
end


-- *****************************************************************************
-- Gall.List.Route
-- *****************************************************************************

---A collection of Gall.Route objects.
---@class Gall.List.Route : Gall.List.Base
---@field innerList Gall.List
Gall.List.Route = Gall.List.Base:newSubclass("Gall.List.Route", Gall.Route)

---Creates a new list, optionally populating it with objects.
---@param iterable any @Optional. The objects to populate the list with. Anything that has an `iter()` function which provides objects which are, or can be used to create, a Gall.Route. E.g. A Gall.Route, an ARDOUR.Route or a route name.
---@return Gall.List.Route
function Gall.List.Route:new(iterable)
  return Gall.List.Route:newInstance(iterable)
end

---Gets the sole object from the list, if there is only one object in the list.
---@return Gall.Route @The object. `nil` if the list contains multiple or no objects.
function Gall.List.Route:getSoleObject()
  return self.innerList:getSoleObject()
end

---Appends a new object to the end of the list.
---@param obj Gall.Route @The object to add
---@return Gall.List.Route @This list (self)
function Gall.List.Route:add(obj)
  return self.innerList:add(obj)
end

---Appends a new object to the end of the list, only if it is not already present.
---@param obj Gall.Route @The object to add
---@return Gall.List.Route @This list (self)
function Gall.List.Route:addUnique(obj)
  return self.innerList:addUnique(obj)
end

---Appends to this list the objects of another list.
---
---The other list can also be a table but only if its keys are numbers 1..n with no gaps in the sequence.
---@param otherList Gall.List.Route | table
---@return Gall.List.Route @This list (self)
function Gall.List.Route:addList(otherList)
  --if it has a gallType property then assume it's a Gall.List subclass, otherwise treat as a table
  if otherList.gallType then
    return self.innerList:addList(otherList.innerList)
  end
  return self.innerList:addList(otherList)
end

---Appends to this list the items retrieved from invoking `iterable:iter()`.
---@param iterable any @Anything that has an `iter()` function which provides objects which are, or can be used to create, a Gall.Route. E.g. A Gall.Route, an ARDOUR.Route or a route name.
---@return Gall.List.Route @This list (self)
function Gall.List.Route:addIter(iterable)
  return self.innerList:addIter(iterable)
end

---Removes this object from the list.
---@param obj Gall.Route @The object to remove
---@return Gall.List.Route @This list (self)
function Gall.List.Route:remove(obj)
  return self.innerList:remove(obj)
end

---Removes from this list the objects retrieved from invoking `iterable:iter()`.
---@param iterable any @Anything that has an `iter()` function which provides objects of type Gall.Route
---@return Gall.List.Route @This list (self)
function Gall.List.Route:removeIter(iterable)
  return self.innerList:removeIter(iterable)
end

---Iterator.
---@return fun(): Gall.Route
function Gall.List.Route:iter()
  return self.innerList:iter()
end

---Returns the index of a given object in this list.
---@param obj Gall.Route @The object to find
---@return integer @The index. `nil` if not found.
function Gall.List.Route:getIndexFor(obj)
  return self.innerList:getIndexFor(obj)
end

---Returns the first object in the list.
---@return Gall.Route @The object. `nil` if the list is empty.
function Gall.List.Route:front()
  return self.innerList:front()
end

---Returns the last object in the list.
---@return Gall.Route @The object. `nil` if the list is empty.
function Gall.List.Route:back()
  return self.innerList:back()
end

---Returns true if this object is in the list.
---@param obj Gall.Route @The object to find
---@return boolean
function Gall.List.Route:contains(obj)
  return self.innerList:contains(obj)
end

---Sorts this list via a less-than/greater-than test on the field specified by `fieldSelectFunc`.
---
---See `Gall.List:sort()` comments for usage examples.
---@param fieldSelectFunc function @Function to specify the field to sort by
---@param descending boolean @If true then sorts in descending order
---@return Gall.List.Route @This list (self)
function Gall.List.Route:sort(fieldSelectFunc, descending)
  return self.innerList:sort(fieldSelectFunc, descending)
end


-- *****************************************************************************
-- Gall.List.Group
-- *****************************************************************************

---A collection of Gall.Group objects.
---@class Gall.List.Group : Gall.List.Base
---@field innerList Gall.List
Gall.List.Group = Gall.List.Base:newSubclass("Gall.List.Group", Gall.Group)

---Creates a new list, optionally populating it with objects.
---@param iterable any @Optional. The objects to populate the list with. Anything that has an `iter()` function which provides objects which are, or can be used to create, a Gall.Group. E.g. A Gall.Group, an ARDOUR.RouteGroup or a group name.
---@return Gall.List.Group
function Gall.List.Group:new(iterable)
  return Gall.List.Group:newInstance(iterable)
end

---Gets the sole object from the list, if there is only one object in the list.
---@return Gall.Group @The object. `nil` if the list contains multiple or no objects.
function Gall.List.Group:getSoleObject()
  return self.innerList:getSoleObject()
end

---Appends a new object to the end of the list.
---@param obj Gall.Group @The object to add
---@return Gall.List.Group @This list (self)
function Gall.List.Group:add(obj)
  return self.innerList:add(obj)
end

---Appends a new object to the end of the list, only if it is not already present.
---@param obj Gall.Group @The object to add
---@return Gall.List.Group @This list (self)
function Gall.List.Group:addUnique(obj)
  return self.innerList:addUnique(obj)
end

---Appends to this list the objects of another list.
---
---The other list can also be a table but only if its keys are numbers 1..n with no gaps in the sequence.
---@param otherList Gall.List.Group | table
---@return Gall.List.Group @This list (self)
function Gall.List.Group:addList(otherList)
  --if it has a gallType property then assume it's a Gall.List subclass, otherwise treat as a table
  if otherList.gallType then
    return self.innerList:addList(otherList.innerList)
  end
  return self.innerList:addList(otherList)
end

---Appends to this list the items retrieved from invoking `iterable:iter()`.
---@param iterable any @Anything that has an `iter()` function which provides objects which are, or can be used to create, a Gall.Group. E.g. A Gall.Group, an ARDOUR.RouteGroup or a group name.
---@return Gall.List.Group @This list (self)
function Gall.List.Group:addIter(iterable)
  return self.innerList:addIter(iterable)
end

---Removes this object from the list.
---@param obj Gall.Group @The object to remove
---@return Gall.List.Group @This list (self)
function Gall.List.Group:remove(obj)
  return self.innerList:remove(obj)
end

---Removes from this list the objects retrieved from invoking `iterable:iter()`.
---@param iterable any @Anything that has an `iter()` function which provides objects of type Gall.Group
---@return Gall.List.Group @This list (self)
function Gall.List.Group:removeIter(iterable)
  return self.innerList:removeIter(iterable)
end

---Iterator.
---@return fun(): Gall.Group
function Gall.List.Group:iter()
  return self.innerList:iter()
end

---Returns the index of a given object in this list.
---@param obj Gall.Group @The object to find
---@return integer @The index. `nil` if not found.
function Gall.List.Group:getIndexFor(obj)
  return self.innerList:getIndexFor(obj)
end

---Returns the first object in the list.
---@return Gall.Group @The object. `nil` if the list is empty.
function Gall.List.Group:front()
  return self.innerList:front()
end

---Returns the last object in the list.
---@return Gall.Group @The object. `nil` if the list is empty.
function Gall.List.Group:back()
  return self.innerList:back()
end

---Returns true if this object is in the list.
---@param obj Gall.Group @The object to find
---@return boolean
function Gall.List.Group:contains(obj)
  return self.innerList:contains(obj)
end

---Sorts this list via a less-than/greater-than test on the field specified by `fieldSelectFunc`.
---
---See `Gall.List:sort()` comments for usage examples.
---@param fieldSelectFunc function @Function to specify the field to sort by
---@param descending boolean @If true then sorts in descending order
---@return Gall.List.Group @This list (self)
function Gall.List.Group:sort(fieldSelectFunc, descending)
  return self.innerList:sort(fieldSelectFunc, descending)
end


-- *****************************************************************************
-- Gall.List.InternalSend
-- *****************************************************************************

---A collection of Gall.InternalSend objects.
---@class Gall.List.InternalSend : Gall.List.Base
---@field innerList Gall.List
Gall.List.InternalSend = Gall.List.Base:newSubclass("Gall.List.InternalSend", Gall.InternalSend)

---Creates a new list, optionally populating it with objects.
---@param iterable any @Optional. The objects to populate the list with. Anything that has an `iter()` function which provides objects which are, or can be used to create, a Gall.InternalSend. E.g. A Gall.InternalSend or an ARDOUR.InternalSend.
---@return Gall.List.InternalSend
function Gall.List.InternalSend:new(iterable)
  return Gall.List.InternalSend:newInstance(iterable)
end

---Gets the sole object from the list, if there is only one object in the list.
---@return Gall.InternalSend @The object. `nil` if the list contains multiple or no objects.
function Gall.List.InternalSend:getSoleObject()
  return self.innerList:getSoleObject()
end

---Appends a new object to the end of the list.
---@param obj Gall.InternalSend @The object to add
---@return Gall.List.InternalSend @This list (self)
function Gall.List.InternalSend:add(obj)
  return self.innerList:add(obj)
end

---Appends a new object to the end of the list, only if it is not already present.
---@param obj Gall.InternalSend @The object to add
---@return Gall.List.InternalSend @This list (self)
function Gall.List.InternalSend:addUnique(obj)
  return self.innerList:addUnique(obj)
end

---Appends to this list the objects of another list.
---
---The other list can also be a table but only if its keys are numbers 1..n with no gaps in the sequence.
---@param otherList Gall.List.InternalSend | table
---@return Gall.List.InternalSend @This list (self)
function Gall.List.InternalSend:addList(otherList)
  --if it has a gallType property then assume it's a Gall.List subclass, otherwise treat as a table
  if otherList.gallType then
    return self.innerList:addList(otherList.innerList)
  end
  return self.innerList:addList(otherList)
end

---Appends to this list the items retrieved from invoking `iterable:iter()`.
---@param iterable any @Anything that has an `iter()` function which provides objects which are, or can be used to create, a Gall.InternalSend. E.g. A Gall.InternalSend or an ARDOUR.InternalSend.
---@return Gall.List.InternalSend @This list (self)
function Gall.List.InternalSend:addIter(iterable)
  return self.innerList:addIter(iterable)
end

---Removes this object from the list.
---@param obj Gall.InternalSend @The object to remove
---@return Gall.List.InternalSend @This list (self)
function Gall.List.InternalSend:remove(obj)
  return self.innerList:remove(obj)
end

---Removes from this list the objects retrieved from invoking `iterable:iter()`.
---@param iterable any @Anything that has an `iter()` function which provides objects of type Gall.InternalSend
---@return Gall.List.InternalSend @This list (self)
function Gall.List.InternalSend:removeIter(iterable)
  return self.innerList:removeIter(iterable)
end

---Iterator.
---@return fun(): Gall.InternalSend
function Gall.List.InternalSend:iter()
  return self.innerList:iter()
end

---Returns the index of a given object in this list.
---@param obj Gall.InternalSend @The object to find
---@return integer @The index. `nil` if not found.
function Gall.List.InternalSend:getIndexFor(obj)
  return self.innerList:getIndexFor(obj)
end

---Returns the first object in the list.
---@return Gall.InternalSend @The object. `nil` if the list is empty.
function Gall.List.InternalSend:front()
  return self.innerList:front()
end

---Returns the last object in the list.
---@return Gall.InternalSend @The object. `nil` if the list is empty.
function Gall.List.InternalSend:back()
  return self.innerList:back()
end

---Returns true if this object is in the list.
---@param obj Gall.InternalSend @The object to find
---@return boolean
function Gall.List.InternalSend:contains(obj)
  return self.innerList:contains(obj)
end

---Sorts this list via a less-than/greater-than test on the field specified by `fieldSelectFunc`.
---
---See `Gall.List:sort()` comments for usage examples.
---@param fieldSelectFunc function @Function to specify the field to sort by
---@param descending boolean @If true then sorts in descending order
---@return Gall.List.InternalSend @This list (self)
function Gall.List.InternalSend:sort(fieldSelectFunc, descending)
  return self.innerList:sort(fieldSelectFunc, descending)
end


-- *****************************************************************************
-- Gall.List.MbSend
-- *****************************************************************************

---A collection of Gall.MbSend objects.
---@class Gall.List.MbSend : Gall.List.Base
---@field innerList Gall.List
Gall.List.MbSend = Gall.List.Base:newSubclass("Gall.List.MbSend", Gall.MbSend)

---Creates a new list, optionally populating it with objects.
---@param iterable any @Optional. The objects to populate the list with. Anything that has an `iter()` function which provides objects of type Gall.MbSend.
---@return Gall.List.MbSend
function Gall.List.MbSend:new(iterable)
  return Gall.List.MbSend:newInstance(iterable)
end

---Gets the sole object from the list, if there is only one object in the list.
---@return Gall.MbSend @The object. `nil` if the list contains multiple or no objects.
function Gall.List.MbSend:getSoleObject()
  return self.innerList:getSoleObject()
end

---Appends a new object to the end of the list.
---@param obj Gall.MbSend @The object to add
---@return Gall.List.MbSend @This list (self)
function Gall.List.MbSend:add(obj)
  return self.innerList:add(obj)
end

---Appends a new object to the end of the list, only if it is not already present.
---@param obj Gall.MbSend @The object to add
---@return Gall.List.MbSend @This list (self)
function Gall.List.MbSend:addUnique(obj)
  return self.innerList:addUnique(obj)
end

---Appends to this list the objects of another list.
---
---The other list can also be a table but only if its keys are numbers 1..n with no gaps in the sequence.
---@param otherList Gall.List.MbSend | table
---@return Gall.List.MbSend @This list (self)
function Gall.List.MbSend:addList(otherList)
  --if it has a gallType property then assume it's a Gall.List subclass, otherwise treat as a table
  if otherList.gallType then
    return self.innerList:addList(otherList.innerList)
  end
  return self.innerList:addList(otherList)
end

---Appends to this list the items retrieved from invoking `iterable:iter()`.
---@param iterable any @Anything that has an `iter()` function which provides objects of type Gall.MbSend.
---@return Gall.List.MbSend @This list (self)
function Gall.List.MbSend:addIter(iterable)
  return self.innerList:addIter(iterable)
end

---Removes this object from the list.
---@param obj Gall.MbSend @The object to remove
---@return Gall.List.MbSend @This list (self)
function Gall.List.MbSend:remove(obj)
  return self.innerList:remove(obj)
end

---Removes from this list the objects retrieved from invoking `iterable:iter()`.
---@param iterable any @Anything that has an `iter()` function which provides objects of type Gall.MbSend
---@return Gall.List.MbSend @This list (self)
function Gall.List.MbSend:removeIter(iterable)
  return self.innerList:removeIter(iterable)
end

---Iterator.
---@return fun(): Gall.MbSend
function Gall.List.MbSend:iter()
  return self.innerList:iter()
end

---Returns the index of a given object in this list.
---@param obj Gall.MbSend @The object to find
---@return integer @The index. `nil` if not found.
function Gall.List.MbSend:getIndexFor(obj)
  return self.innerList:getIndexFor(obj)
end

---Returns the first object in the list.
---@return Gall.MbSend @The object. `nil` if the list is empty.
function Gall.List.MbSend:front()
  return self.innerList:front()
end

---Returns the last object in the list.
---@return Gall.MbSend @The object. `nil` if the list is empty.
function Gall.List.MbSend:back()
  return self.innerList:back()
end

---Returns true if this object is in the list.
---@param obj Gall.MbSend @The object to find
---@return boolean
function Gall.List.MbSend:contains(obj)
  return self.innerList:contains(obj)
end

---Sorts this list via a less-than/greater-than test on the field specified by `fieldSelectFunc`.
---
---See `Gall.List:sort()` comments for usage examples.
---@param fieldSelectFunc function @Function to specify the field to sort by
---@param descending boolean @If true then sorts in descending order
---@return Gall.List.MbSend @This list (self)
function Gall.List.MbSend:sort(fieldSelectFunc, descending)
  return self.innerList:sort(fieldSelectFunc, descending)
end


-- *****************************************************************************
-- Gall.List.Processor
-- *****************************************************************************

---A collection of Gall.Processor objects.
---@class Gall.List.Processor : Gall.List.Base
---@field innerList Gall.List
Gall.List.Processor = Gall.List.Base:newSubclass("Gall.List.Processor", Gall.Processor)

---Creates a new list, optionally populating it with objects.
---@param iterable any @Optional. The objects to populate the list with. Anything that has an `iter()` function which provides objects which are, or can be used to create, a Gall.Processor. E.g. A Gall.Processor or an ARDOUR.Processor.
---@return Gall.List.Processor
function Gall.List.Processor:new(iterable)
  return Gall.List.Processor:newInstance(iterable)
end

---Gets the sole object from the list, if there is only one object in the list.
---@return Gall.Processor @The object. `nil` if the list contains multiple or no objects.
function Gall.List.Processor:getSoleObject()
  return self.innerList:getSoleObject()
end

---Appends a new object to the end of the list.
---@param obj Gall.Processor @The object to add
---@return Gall.List.Processor @This list (self)
function Gall.List.Processor:add(obj)
  return self.innerList:add(obj)
end

---Appends a new object to the end of the list, only if it is not already present.
---@param obj Gall.Processor @The object to add
---@return Gall.List.Processor @This list (self)
function Gall.List.Processor:addUnique(obj)
  return self.innerList:addUnique(obj)
end

---Appends to this list the objects of another list.
---
---The other list can also be a table but only if its keys are numbers 1..n with no gaps in the sequence.
---@param otherList Gall.List.Processor | table
---@return Gall.List.Processor @This list (self)
function Gall.List.Processor:addList(otherList)
  --if it has a gallType property then assume it's a Gall.List subclass, otherwise treat as a table
  if otherList.gallType then
    return self.innerList:addList(otherList.innerList)
  end
  return self.innerList:addList(otherList)
end

---Appends to this list the items retrieved from invoking `iterable:iter()`.
---@param iterable any @Anything that has an `iter()` function which provides objects which are, or can be used to create, a Gall.Processor. E.g. A Gall.Processor or an ARDOUR.Processor.
---@return Gall.List.Processor @This list (self)
function Gall.List.Processor:addIter(iterable)
  return self.innerList:addIter(iterable)
end

---Removes this object from the list.
---@param obj Gall.Processor @The object to remove
---@return Gall.List.Processor @This list (self)
function Gall.List.Processor:remove(obj)
  return self.innerList:remove(obj)
end

---Removes from this list the objects retrieved from invoking `iterable:iter()`.
---@param iterable any @Anything that has an `iter()` function which provides objects which are, or can be used to create, a Gall.Processor. E.g. A Gall.Processor or an ARDOUR.Processor.
---@return Gall.List.Processor @This list (self)
function Gall.List.Processor:removeIter(iterable)
  return self.innerList:removeIter(iterable)
end

---Iterator.
---@return fun(): Gall.Processor
function Gall.List.Processor:iter()
  return self.innerList:iter()
end

---Returns the index of a given object in this list.
---@param obj Gall.Processor @The object to find
---@return integer @The index. `nil` if not found.
function Gall.List.Processor:getIndexFor(obj)
  return self.innerList:getIndexFor(obj)
end

---Returns the first object in the list.
---@return Gall.Processor @The object. `nil` if the list is empty.
function Gall.List.Processor:front()
  return self.innerList:front()
end

---Returns the last object in the list.
---@return Gall.Processor @The object. `nil` if the list is empty.
function Gall.List.Processor:back()
  return self.innerList:back()
end

---Returns true if this object is in the list.
---@param obj Gall.Processor @The object to find
---@return boolean
function Gall.List.Processor:contains(obj)
  return self.innerList:contains(obj)
end

---Sorts this list via a less-than/greater-than test on the field specified by `fieldSelectFunc`.
---
---See `Gall.List:sort()` comments for usage examples.
---@param fieldSelectFunc function @Function to specify the field to sort by
---@param descending boolean @If true then sorts in descending order
---@return Gall.List.Processor @This list (self)
function Gall.List.Processor:sort(fieldSelectFunc, descending)
  return self.innerList:sort(fieldSelectFunc, descending)
end


-- *****************************************************************************
-- Gall.List.Port
-- *****************************************************************************

---A collection of Gall.Port objects.
---@class Gall.List.Port : Gall.List.Base
---@field innerList Gall.List
Gall.List.Port = Gall.List.Base:newSubclass("Gall.List.Port", Gall.Port)

---Creates a new list, optionally populating it with objects.
---@param iterable any @Optional. The objects to populate the list with. Anything that has an `iter()` function which provides objects which are, or can be used to create, a Gall.Port. E.g. A Gall.Port, an ARDOUR.Port or a port name.
---@return Gall.List.Port
function Gall.List.Port:new(iterable)
  return Gall.List.Port:newInstance(iterable)
end

---Gets the sole object from the list, if there is only one object in the list.
---@return Gall.Port @The object. `nil` if the list contains multiple or no objects.
function Gall.List.Port:getSoleObject()
  return self.innerList:getSoleObject()
end

---Appends a new object to the end of the list.
---@param obj Gall.Port @The object to add
---@return Gall.List.Port @This list (self)
function Gall.List.Port:add(obj)
  return self.innerList:add(obj)
end

---Appends a new object to the end of the list, only if it is not already present.
---@param obj Gall.Port @The object to add
---@return Gall.List.Port @This list (self)
function Gall.List.Port:addUnique(obj)
  return self.innerList:addUnique(obj)
end

---Appends to this list the objects of another list.
---
---The other list can also be a table but only if its keys are numbers 1..n with no gaps in the sequence.
---@param otherList Gall.List.Port | table
---@return Gall.List.Port @This list (self)
function Gall.List.Port:addList(otherList)
  --if it has a gallType property then assume it's a Gall.List subclass, otherwise treat as a table
  if otherList.gallType then
    return self.innerList:addList(otherList.innerList)
  end
  return self.innerList:addList(otherList)
end

---Appends to this list the items retrieved from invoking `iterable:iter()`.
---@param iterable any @Anything that has an `iter()` function which provides objects which are, or can be used to create, a Gall.Port. E.g. A Gall.Port, an ARDOUR.Port or a port name.
---@return Gall.List.Port @This list (self)
function Gall.List.Port:addIter(iterable)
  return self.innerList:addIter(iterable)
end

---Removes this object from the list.
---@param obj Gall.Port @The object to remove
---@return Gall.List.Port @This list (self)
function Gall.List.Port:remove(obj)
  return self.innerList:remove(obj)
end

---Removes from this list the objects retrieved from invoking `iterable:iter()`.
---@param iterable any @Anything that has an `iter()` function which provides objects of type Gall.Port
---@return Gall.List.Port @This list (self)
function Gall.List.Port:removeIter(iterable)
  return self.innerList:removeIter(iterable)
end

---Iterator.
---@return fun(): Gall.Port
function Gall.List.Port:iter()
  return self.innerList:iter()
end

---Returns the index of a given object in this list.
---@param obj Gall.Port @The object to find
---@return integer @The index. `nil` if not found.
function Gall.List.Port:getIndexFor(obj)
  return self.innerList:getIndexFor(obj)
end

---Returns the first object in the list.
---@return Gall.Port @The object. `nil` if the list is empty.
function Gall.List.Port:front()
  return self.innerList:front()
end

---Returns the last object in the list.
---@return Gall.Port @The object. `nil` if the list is empty.
function Gall.List.Port:back()
  return self.innerList:back()
end

---Returns true if this object is in the list.
---@param obj Gall.Port @The object to find
---@return boolean
function Gall.List.Port:contains(obj)
  return self.innerList:contains(obj)
end

---Sorts this list via a less-than/greater-than test on the field specified by `fieldSelectFunc`.
---
---See `Gall.List:sort()` comments for usage examples.
---@param fieldSelectFunc function @Function to specify the field to sort by
---@param descending boolean @If true then sorts in descending order
---@return Gall.List.Port @This list (self)
function Gall.List.Port:sort(fieldSelectFunc, descending)
  return self.innerList:sort(fieldSelectFunc, descending)
end


-- *****************************************************************************
-- Gall.List.Vca
-- *****************************************************************************

---A collection of Gall.Vca objects.
---@class Gall.List.Vca : Gall.List.Base
---@field innerList Gall.List
Gall.List.Vca = Gall.List.Base:newSubclass("Gall.List.Vca", Gall.Vca)

---Creates a new list, optionally populating it with objects.
---@param iterable any @Optional. The objects to populate the list with. Anything that has an `iter()` function which provides objects which are, or can be used to create, a Gall.Vca. E.g. A Gall.Vca, an ARDOUR.VCA or a vca name.
---@return Gall.List.Vca
function Gall.List.Vca:new(iterable)
  return Gall.List.Vca:newInstance(iterable)
end

---Gets the sole object from the list, if there is only one object in the list.
---@return Gall.Vca @The object. `nil` if the list contains multiple or no objects.
function Gall.List.Vca:getSoleObject()
  return self.innerList:getSoleObject()
end

---Appends a new object to the end of the list.
---@param obj Gall.Vca @The object to add
---@return Gall.List.Vca @This list (self)
function Gall.List.Vca:add(obj)
  return self.innerList:add(obj)
end

---Appends a new object to the end of the list, only if it is not already present.
---@param obj Gall.Vca @The object to add
---@return Gall.List.Vca @This list (self)
function Gall.List.Vca:addUnique(obj)
  return self.innerList:addUnique(obj)
end

---Appends to this list the objects of another list.
---
---The other list can also be a table but only if its keys are numbers 1..n with no gaps in the sequence.
---@param otherList Gall.List.Vca | table
---@return Gall.List.Vca @This list (self)
function Gall.List.Vca:addList(otherList)
  --if it has a gallType property then assume it's a Gall.List subclass, otherwise treat as a table
  if otherList.gallType then
    return self.innerList:addList(otherList.innerList)
  end
  return self.innerList:addList(otherList)
end

---Appends to this list the items retrieved from invoking `iterable:iter()`.
---@param iterable any @Anything that has an `iter()` function which provides objects which are, or can be used to create, a Gall.Vca. E.g. A Gall.Vca, an ARDOUR.VCA or a vca name.
---@return Gall.List.Vca @This list (self)
function Gall.List.Vca:addIter(iterable)
  return self.innerList:addIter(iterable)
end

---Removes this object from the list.
---@param obj Gall.Vca @The object to remove
---@return Gall.List.Vca @This list (self)
function Gall.List.Vca:remove(obj)
  return self.innerList:remove(obj)
end

---Removes from this list the objects retrieved from invoking `iterable:iter()`.
---@param iterable any @Anything that has an `iter()` function which provides objects of type Gall.Vca
---@return Gall.List.Vca @This list (self)
function Gall.List.Vca:removeIter(iterable)
  return self.innerList:removeIter(iterable)
end

---Iterator.
---@return fun(): Gall.Vca
function Gall.List.Vca:iter()
  return self.innerList:iter()
end

---Returns the index of a given object in this list.
---@param obj Gall.Vca @The object to find
---@return integer @The index. `nil` if not found.
function Gall.List.Vca:getIndexFor(obj)
  return self.innerList:getIndexFor(obj)
end

---Returns the first object in the list.
---@return Gall.Vca @The object. `nil` if the list is empty.
function Gall.List.Vca:front()
  return self.innerList:front()
end

---Returns the last object in the list.
---@return Gall.Vca @The object. `nil` if the list is empty.
function Gall.List.Vca:back()
  return self.innerList:back()
end

---Returns true if this object is in the list.
---@param obj Gall.Vca @The object to find
---@return boolean
function Gall.List.Vca:contains(obj)
  return self.innerList:contains(obj)
end

---Sorts this list via a less-than/greater-than test on the field specified by `fieldSelectFunc`.
---
---See `Gall.List:sort()` comments for usage examples.
---@param fieldSelectFunc function @Function to specify the field to sort by
---@param descending boolean @If true then sorts in descending order
---@return Gall.List.Vca @This list (self)
function Gall.List.Vca:sort(fieldSelectFunc, descending)
  return self.innerList:sort(fieldSelectFunc, descending)
end
