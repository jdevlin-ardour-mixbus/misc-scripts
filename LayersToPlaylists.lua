ardour { 
	["type"] = "EditorAction",
	name = "(jd) Layers To Playlists",
	license     = "MIT",
	author      = "John Devlin",
	description = [[For each layer of the selected track's current playlist, create a new playlist and copy into it the layer's regions.]]
}

function factory () return function ()

	local dlgTitle = "Layers to Playlist"

	function dlgMsg(text)
		local md = LuaDialog.Message(dlgTitle, text, LuaDialog.MessageType.Info, LuaDialog.ButtonType.Close)
		md:run()
		md = nil
		collectgarbage()
	end

	function dlgPrompt(text)
		local md = LuaDialog.Message (dlgTitle, text, LuaDialog.MessageType.Question, LuaDialog.ButtonType.Yes_No)
		local rv = md:run()
		md = nil
		collectgarbage()
		return rv == LuaDialog.Response.Yes
	end

	local selRoutes = Editor:get_selection().tracks:routelist()

	if selRoutes:size() ~= 1 then
		dlgMsg("Multiple or no tracks selected.\n\nSelect (only) the audio track to perform the action on.")
		return
	end

	local selRoute = selRoutes:front()

	if selRoute:data_type():to_string() ~= "audio" then
		dlgMsg("Selection is not an audio track")
		return
	end

	local track = selRoute:to_track()

	if track:isnil() then
		-- must be a bus
		dlgMsg("Selection is a bus, not an audio track")
		return
	end


	local plOrig = track:playlist()

	local mapLayerRegions = {} --map of regions by layer

	--build the map
	for rg in plOrig:region_list():iter() do
		local layer = rg:layer() + 1  -- index the layers starting at 1 instead of 0
		if mapLayerRegions[layer] == nil then
			mapLayerRegions[layer] = {}
		end
		table.insert(mapLayerRegions[layer], rg)
	end

	if #mapLayerRegions <= 1 then
		-- 0 or 1 layers; nothing to do
		dlgMsg("The current playlist has no layers or only one layer. Nothing to action.")
		return
	end

	local msg = "There is no undo for this action. Saving the session before doing this would be wise, in case something goes wrong.\n\nContinue? (It will NOT auto-save the session for you.)"
	if not dlgPrompt(msg) then return end

	-- debug: print the map
	-- for layer, regions in ipairs(mapLayerRegions) do
	--   print("layer "..layer)
	--   for _, rg in ipairs(regions) do print("  > "..rg:name()) end
	-- end

	for layer, regions in ipairs(mapLayerRegions) do
		-- create a new playlist for this layer; track:playlist() will then be this new playlist
		track:use_new_playlist(ARDOUR.DataType:audio())

		-- copy this layer's regions to the new playlist
		for _, rg in ipairs(regions) do
			--plOrig:remove_region(rg)  --remove from the original playlist if desired
			track:playlist():add_region(rg, rg:position(), 1, false, 0, 0, false)
		end
	end

	collectgarbage()

end end

function icon (params) return function (ctx, width, height, fg)
	local iconText = "L>P"
	local iconFont = "ArdourMono"
	local iconFontScale = 0.5

	local txt = Cairo.PangoLayout (ctx, iconFont.." ".. math.ceil(math.min(width, height) * iconFontScale) .. "px")
	txt:set_text (iconText)
	local tw, th = txt:get_pixel_size ()
	ctx:move_to (.5 * (width - tw), .5 * (height - th))
	ctx:set_source_rgba(ARDOUR.LuaAPI.color_to_rgba(fg))
	txt:show_in_cairo_context (ctx)
end end
