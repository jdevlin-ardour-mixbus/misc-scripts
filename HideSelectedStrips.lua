ardour {
  ["type"] = "EditorAction",
  name = "Hide Selected Strips",
  license     = "MIT",
  author      = "John Devlin",
  description = [[Hides all selected strips. Not VCAs. For Mixbusses this only affects the Editor not the Mixer.]]
}

function factory (params) return function ()

  for r in Session:get_routes():iter() do
    local rtav = Editor:rtav_from_route(r)
    if rtav then
      Editor:hide_track_in_display(rtav:to_timeaxisview(), true)
    end
  end

end end

function icon (params) return function (ctx, width, height, fg)

  local iconText = "HS"
  local iconFont = "ArdourMono"
  local iconFontScale = 0.5

  local txt = Cairo.PangoLayout (ctx, iconFont.." ".. math.ceil(math.min(width, height) * iconFontScale) .. "px")
  txt:set_text (iconText)
  local tw, th = txt:get_pixel_size ()
  ctx:move_to (.5 * (width - tw), .5 * (height - th))
  ctx:set_source_rgba (ARDOUR.LuaAPI.color_to_rgba (fg))
  txt:show_in_cairo_context (ctx)
end end
