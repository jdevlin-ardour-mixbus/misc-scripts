ardour {
  ["type"] = "EditorAction",
  name = "(jd) Move Mixbusses v1.0",
  license     = "MIT",
  author      = "John Devlin",
  description = [[This effectively 'moves' Mixbusses by moving everything about them to different 
positions, including settings, processors and automation.
Requires: Gall v3.0 or later.
  ]]
}

function factory (params) return function ()

  local fgall, err = loadfile(ARDOUR.user_config_directory(-1).."/scripts/lib/_gall.lua"); if (err) then error(err) else fgall() end

  local doTrace = false

  local PTB_NAME = "-temp MMB-"
  local isUsingPtb = false

  function trace(msg)
    if doTrace then
      print(msg)
      --Gall.Util:sleep(1000)
    end
  end

  ---@param gr Gall.Route
  ---@param displayName string
  function getProcessorVisiblePosition(gr, displayName)

    local i = 1
    for p in gr:getProcessors():iter() do
      if p.ao:display_to_user() then
        if p.ao:display_name() == displayName then return i end
        i = i + 1
      end
    end
  end

  ---If there are multiple processors on this route with the same name as this processor
  ---then it returns which one it is. 1-based. Returns 1 if no duplicate names found, or this is the first.
  ---@param gr Gall.Route
  ---@param gp Gall.Processor
  ---@return integer
  function getProcessorOccurrenceNumber(gr, gp)

    local count = 0
    local thisName = gp.ao:name()

    for p in gr:getProcessors():iter() do
      if p.ao:name() == thisName then
        count = count + 1
        if p.ao == gp.ao then
          return count
        end
      end
    end

    return 1
  end

  ---Gets the processor on this route with this name and the occurrenceNumber as per getProcessorOccurrenceNumber().
  ---@param gr Gall.Route
  ---@param name string
  ---@param occurrenceNumber integer
  ---@return Gall.Processor
  function getProcessorFromNameAndOccurrenceNumber(gr, name, occurrenceNumber)

    local count = 0

    for p in gr:getProcessors():iter() do
      if p.ao:name() == name then
        count = count + 1
        if count == occurrenceNumber then
          return p
        end
      end
    end

  end

  local Auto = {
    Fader = 1,
    Trim = 2,
    Mute = 3,
    TapeDrive = 4,
    StereoWidth = 5,
    EqEnable = 6,
    EqHi = 7,
    EqMid = 8,
    EqLow = 9,
    CompEnable = 10,
    CompThresh = 11,
    CompSpeed = 12,
    CompMode = 13,
    CompMakeup = 14,
    MasterAssign = 15,
    Pan = 16,
    Send9Assign = 17,
    Send9Level = 18,
    Send9PanEnable = 19,
    Send9Pan = 20,
    Send10Assign = 21,
    Send10Level = 22,
    Send10PanEnable = 23,
    Send10Pan = 24,
    Send11Assign = 25,
    Send11Level = 26,
    Send11PanEnable = 27,
    Send11Pan = 28,
    Send12Assign = 29,
    Send12Level = 30,
    Send12PanEnable = 31,
    Send12Pan = 32
  }

  ---@param auto integer
  ---@param gr Gall.Route
  ---@return ARDOUR.AutomationControl
  function getAutomationControl(auto, gr)

    ---@type ARDOUR.AutomationControl
    local ac = nil

    local is32c = Gall.App:is32c()

    if auto == Auto.Fader then
      ac = gr.ao:gain_control()
    elseif auto == Auto.Trim then
      ac = gr.ao:trim_control()
    elseif auto == Auto.Mute then
      ac = gr.ao:mute_control()
    elseif auto == Auto.TapeDrive then
      ac = gr:getPluginInsertsForName("Input Stage"):getSoleObject():getPluginAutomationControl("Tape Drive")
    elseif auto == Auto.StereoWidth then
      ac = gr:getPluginInsertsForName("Input Stage"):getSoleObject():getPluginAutomationControl("Stereo Width")
    elseif auto == Auto.EqEnable then
      ac = gr.ao:eq_enable_controllable()
    elseif auto == Auto.EqHi then
      ac = gr.ao:eq_gain_controllable(0)
    elseif auto == Auto.EqMid then
      ac = gr.ao:eq_gain_controllable(1)
    elseif auto == Auto.EqLow then
      ac = gr.ao:eq_gain_controllable(2)
    elseif auto == Auto.CompEnable then
      ac = gr.ao:comp_enable_controllable()
    elseif auto == Auto.CompThresh then
      ac = gr.ao:comp_threshold_controllable()
    elseif auto == Auto.CompSpeed then
      ac = gr.ao:comp_speed_controllable()
    elseif auto == Auto.CompMode then
      ac = gr.ao:comp_mode_controllable()
    elseif auto == Auto.CompMakeup then
      ac = gr.ao:comp_makeup_controllable()
    elseif auto == Auto.MasterAssign then
      ac = gr.ao:master_send_enable_controllable()
    elseif auto == Auto.Pan then
      ac = gr.ao:pan_azimuth_control()

    elseif auto == Auto.Send9Assign then
      ac = gr.ao:send_enable_controllable(0)
    elseif auto == Auto.Send9PanEnable and is32c then
      ac = gr.ao:send_pan_azimuth_enable_controllable(0)
    elseif auto == Auto.Send9Pan and is32c then
      ac = gr.ao:send_pan_azimuth_controllable(0)

    elseif auto == Auto.Send10Assign then
      ac = gr.ao:send_enable_controllable(1)
    elseif auto == Auto.Send10PanEnable and is32c then
      ac = gr.ao:send_pan_azimuth_enable_controllable(1)
    elseif auto == Auto.Send10Pan and is32c then
      ac = gr.ao:send_pan_azimuth_controllable(1)

    elseif auto == Auto.Send11Assign then
      ac = gr.ao:send_enable_controllable(2)
    elseif auto == Auto.Send11PanEnable and is32c then
      ac = gr.ao:send_pan_azimuth_enable_controllable(2)
    elseif auto == Auto.Send11Pan and is32c then
      ac = gr.ao:send_pan_azimuth_controllable(2)
      
    elseif auto == Auto.Send12Assign then
      ac = gr.ao:send_enable_controllable(3)
     elseif auto == Auto.Send12PanEnable and is32c then
      ac = gr.ao:send_pan_azimuth_enable_controllable(3)
    elseif auto == Auto.Send12Pan and is32c then
      ac = gr.ao:send_pan_azimuth_controllable(3)

    else
      --SendLevel 9..12

      --An additional check here. If this route is MB 9..12 (or this is not 32C) then it has no MB sends. 
      --But if it has other kinds of sends then send_level_controllable() can return the controller for those 
      --instead. We don't want that here. These SendLevel auto types are meant for MB Sends only.

      if Gall.App:is32c() and gr:isMb1to8() then
        if auto == Auto.Send9Level then
          ac = gr.ao:send_level_controllable(0)
        elseif auto == Auto.Send10Level then
          ac = gr.ao:send_level_controllable(1)
        elseif auto == Auto.Send11Level then
          ac = gr.ao:send_level_controllable(2)
        elseif auto == Auto.Send12Level then
          ac = gr.ao:send_level_controllable(3)
        end
      end

    end

    if not ac or ac:isnil() then
      return nil
    end

    return ac
  end

  ---@param gr Gall.Route
  ---@return table
  function getMixbusSettings(gr)

    local t = {}
    local r = gr.ao

    t["name"] = r:name()
    t["comment"] = r:comment()
    t["color"] = r:presentation_info_ptr():color()

    t["eqEnable"] = r:eq_enable_controllable():get_value()
    t["eqGainLow"] = r:eq_gain_controllable(0):get_value()
    t["eqGainMid"] = r:eq_gain_controllable(1):get_value()
    t["eqGainHi"] = r:eq_gain_controllable(2):get_value()

    t["stereoWidth"] = gr:mbGetStereoWidthValue()
    t["tapeDrive"] = gr:mbGetTapeDriveValue()

    if Gall.App:is32c() and not gr:isMb9to12() then
      for i = 0,3 do
        t["send"..i] = {
          ["enable"] = r:send_enable_controllable(i):get_value(),
          ["level"] = r:send_level_controllable(i):get_value(),
          ["panEnable"] = r:send_pan_azimuth_enable_controllable(i):get_value(),
          ["pan"] = r:send_pan_azimuth_controllable(i):get_value(),
        }
      end
    end

    t["pan"] = r:pan_azimuth_control():get_value()
    t["master"] = r:master_send_enable_controllable():get_value()
    t["mute"] = r:mute_control():get_value()

    t["trim"] = r:trim_control():get_value()
    t["fader"] = r:gain_control():get_value()

    t["compEnable"] = r:comp_enable_controllable():get_value()
    t["compMakeup"] = r:comp_makeup_controllable():get_value()
    t["compMode"] = r:comp_mode_controllable():get_value()
    t["compSpeed"] = r:comp_speed_controllable():get_value()  -- attack/ratio/release
    t["compThreshold"] = r:comp_threshold_controllable():get_value()

    t["group"] = gr:getGroup()
    t["vcas"] = gr:getVcaMasters()
    t["phaseL"] = r:phase_control():inverted(0)
    t["phaseR"] = r:phase_control():inverted(1)

    t["procPosFader"] = getProcessorVisiblePosition(gr, "Fader")
    t["procPosEq"] = getProcessorVisiblePosition(gr, "EQ")
    t["procPosComp"] = getProcessorVisiblePosition(gr, "Comp")

    --mixbus sends (not used - setAllMbSends() handles this)
    -- t["mbSends"] = {}
    -- for mbs in gr:mbGetSendsToThis(true):iter() do
    --   local rs = mbs:getSource().ao
    --   local i = mbs:getIndex()
    --   table.insert(t["mbSends"], {
    --     ["route"] = rs:name(),
    --     ["level"] = rs:send_level_controllable(i):get_value(),
    --     ["panEnable"] = rs:send_pan_azimuth_enable_controllable(i):get_value(),
    --     ["pan"] = rs:send_pan_azimuth_controllable(i):get_value(),
    --   })
    -- end

    --foldback sends
    t["foldbackSends"] = {}
    for s in gr:getInternalSends():iter() do
      if s.ao:is_foldback() then
        local tfbs = {
          ["targetName"] = s:getTarget().ao:name(),
          ["isPostFade"] = gr:isProcessorPostFade(s),
          ["gain"] = s.ao:gain_control():get_value(),
          ["active"] = s.ao:active()
        }
        table.insert(t["foldbackSends"], tfbs)
      end
    end

    --PluginInserts (instance and pin mapping info)
    t["pluginInserts"] = {}
    for p in gr:getPluginInserts():iter() do
      local pi = p:pi()
      local tis = {}
      for i = 0, pi:get_count()-1 do
        table.insert(tis, pi:input_map(i))
      end
      local tos = {}
      for i = 0, pi:get_count()-1 do
        table.insert(tos, pi:output_map(i))
      end
      table.insert(t["pluginInserts"], {
        ["thisProcName"] = p.ao:name(),
        ["thisProcNumber"] = getProcessorOccurrenceNumber(gr, p),
        ["instances"] = pi:get_count(),
        ["inPorts"] = pi:input_streams(),
        ["outPorts"] = pi:output_streams(),
        ["inMaps"] = tis,
        ["outMaps"] = tos
      })
    end

    --sidechains
    --all sends on other routes feeding processor sidechains on this MB
    t["sidechainSendsTo"] = {}
    for p in gr:getPluginInserts():iter() do
      for s in p:getSidechainSendsToThis():iter() do
        table.insert(t["sidechainSendsTo"], {
          ["thisProcName"] = p.ao:name(),
          ["thisProcNumber"] = getProcessorOccurrenceNumber(gr, p),
          ["routeName"] = s:getRoute().ao:name(),
          ["gain"] = s.ao:to_send():gain_control():get_value(),
          ["active"] = s.ao:active(),
          ["isPostFade"] = s:isPostFade()
        })
      end
    end
    --all routes feeding processor sidechains on this MB directly (via their outputs not SC sends)
    t["sidechainRoutesTo"] = {}
    for p in gr:getPluginInserts():iter() do
      for rt in p:getSidechainRoutesToThis():iter() do
        table.insert(t["sidechainRoutesTo"], {
          ["thisProcName"] = p.ao:name(),
          ["thisProcNumber"] = getProcessorOccurrenceNumber(gr, p),
          ["routeName"] = rt.ao:name()
        })
      end
    end

    --output connections
    t["outputConnections"] = {}
    for op in gr:getPorts("out", "audio"):iter() do
      local tconns = {}
      for cp in op:getConnectedTo():iter() do
        table.insert(tconns, cp.ao:name())
      end
      table.insert(t["outputConnections"], tconns)
    end

    --t["meteringPoint"] = ""  --can be set but can't be read - there is no get_meter_point() method

    --automation state (Write, Play, etc.)
    --Store the current state of all automation controls and set them to Manual. If any were left in Write then 
    --their automation would capture the set-value actions this script performs, e.g. setting fader level would create
    --a Fader automation event, which we do not want to do.
    t["autoStates"] = {}
    for _, autoType in pairs(Auto) do
      local ac = getAutomationControl(autoType, gr)
      if ac then
        table.insert(t["autoStates"], {
          autoType = autoType,
          state = ac:automation_state(),
          interpolation = ac:alist():interpolation()
        })
        ac:set_automation_state(ARDOUR.AutoState.Off)
      end
    end

    return t
  end

  ---@param gr Gall.Route @The MB to set
  ---@param settings table @The settings of the swapped-with MB
  ---@param name string @New name of the MB
  ---@param oldNumber integer @The number of this MB before the swap
  function setMixbusSettings(gr, settings, name, oldNumber)

    local t = settings
    local r = gr.ao

    r:set_name(name)
    r:set_comment(t["comment"], nil)
    r:presentation_info_ptr():set_color(t["color"])

    r:eq_gain_controllable(0):set_value(t["eqGainLow"], PBD.GroupControlDisposition.NoGroup)
    r:eq_gain_controllable(1):set_value(t["eqGainMid"], PBD.GroupControlDisposition.NoGroup)
    r:eq_gain_controllable(2):set_value(t["eqGainHi"], PBD.GroupControlDisposition.NoGroup)
    --do last because the above will activate it
    r:eq_enable_controllable():set_value(t["eqEnable"], PBD.GroupControlDisposition.NoGroup)

    gr:mbSetStereoWidthValue(t["stereoWidth"])
    gr:mbSetTapeDriveValue(t["tapeDrive"])

    --MB sends...
    --If this MB was 1..8:
    -- - If is now 9..12 then it now has no sends; do nothing.
    -- - If is still 1..8 then it swapped with another 1..8; apply the captured send values.
    --If this MB was 9..12:
    -- - If is now 1..8 then it didn't have sends before; the sends it now has should all be turned off.
    -- - If is still 9..12 then it still doesn't have sends; do nothing.
    
    if Gall.App:is32c() and not gr:isMb9to12() then
      if oldNumber >= 9 then
        for i = 9,12 do
          gr:getMbSend(i):reset()
        end
      else
        for i = 0,3 do
          local t2 = t["send"..i]
          r:send_level_controllable(i):set_value(t2["level"], PBD.GroupControlDisposition.NoGroup)
          r:send_pan_azimuth_controllable(i):set_value(t2["pan"], PBD.GroupControlDisposition.NoGroup)
          --do last because the above will activate it
          r:send_pan_azimuth_enable_controllable(i):set_value(t2["panEnable"], PBD.GroupControlDisposition.NoGroup)
          r:send_enable_controllable(i):set_value(t2["enable"], PBD.GroupControlDisposition.NoGroup)
        end
      end
    end

    r:pan_azimuth_control():set_value(t["pan"], PBD.GroupControlDisposition.NoGroup)
    r:master_send_enable_controllable():set_value(t["master"], PBD.GroupControlDisposition.NoGroup)
    r:mute_control():set_value(t["mute"], PBD.GroupControlDisposition.NoGroup)
    
    r:trim_control():set_value(t["trim"], PBD.GroupControlDisposition.NoGroup)
    r:gain_control():set_value(t["fader"], PBD.GroupControlDisposition.NoGroup)

    r:comp_makeup_controllable():set_value(t["compMakeup"], PBD.GroupControlDisposition.NoGroup)
    r:comp_mode_controllable():set_value(t["compMode"], PBD.GroupControlDisposition.NoGroup)
    r:comp_speed_controllable():set_value(t["compSpeed"], PBD.GroupControlDisposition.NoGroup)
    r:comp_threshold_controllable():set_value(t["compThreshold"], PBD.GroupControlDisposition.NoGroup)
    --do last because the above will activate it
    r:comp_enable_controllable():set_value(t["compEnable"], PBD.GroupControlDisposition.NoGroup)

    local group = Gall.Groups:get(t["group"])
    if group then
      group.ao:add(r)
    end

    --remove all current VCA masters
    for vca in gr:getVcaMasters():iter() do
      r:to_slavable():unassign(vca.ao)
    end
    --add VCA masters
    for vca in Gall.List.Vca:new():addList(t["vcas"]):iter() do
      r:to_slavable():assign(vca.ao)
    end

    r:phase_control():set_phase_invert(0, t["phaseL"])
    r:phase_control():set_phase_invert(1, t["phaseR"])
  end

  function createPluginTransferBus()

    local busName = PTB_NAME

    if Gall.Routes:get(busName) ~= nil then
      return Gall.Dlg:cannotProceed("Audio bus \""..busName .."\" already exists. It must be removed before this operation can be performed.")
    end

    local bus = Session:new_audio_route (
      2, -- in channel count
      2, -- out channel count
      nil, -- group (type = RouteGroup) - add to group as last step so as not to affect other group members when adjusting mb sends etc.
      1,   -- number of them to create
      busName, -- track name
      ARDOUR.PresentationInfo.Flag.AudioBus,
      ARDOUR.PresentationInfo.max_order  -- track position (0 = first, ARDOUR.PresentationInfo.max_order = last)
    ):front()
    local gr = Gall.Routes:get(bus)

    return gr
  end

  ---Gets the Plugin Transfer Bus.
  ---@return Gall.Route
  function getPtb()
    --[[
      Originally I was just using the route object returned by createPluginTransferBus(), passing it
      to swap() and also using it in the PTB removal step. But this was causing app crashes. Those crashes were
      traced to the Gall.Route:select() calls and in there it was traced to ARDOUR.Route:is_selected().
      
      The crashes would only happen if 'a lot of stuff' was happening between PTB's creation and the select() 
      call, e.g. getMixbusSettings() calls - its getSidechainRoutesToThis() calls seemed to be the culprit there. 
      Also the PTB removal step (selection) would crash if setAllMbSends() was called before it - its mbGetSendsToThis() 
      calls seemed to be the main culprit there but not always the only one.

      I think that the Route in PTB.ao was getting 'lost' somehow (weak pointers?), so ptb.ao:is_selected() was being 
      called on something that no longer existed, hence the crash. Tracing showed that PTB.ao:name() was an empty string, 
      which was the clue that PTB.ao was getting lost.
      
      So as an alternative, if the PTB is needed then use this function. It gets a fresh reference to that route.
    ]]
    return Gall.Routes:get(PTB_NAME)
  end

---Has any processors which can be moved via "cut/paste-processors" action.
---@param gr Gall.Route
---@return boolean
  function hasMoveableProcessors(gr)

    -- Moveable = plugin insert, internal send, external send, sidechain send

    --to_plugininsert() -> plugin inserts, "EQ", "Comp", "Input Stage"
    --to_send() -> internal sends, external sends, sidechain sends, foldback sends, Monitor
    --display_to_user() -> if false then excludes some the above: "Input Stage", foldback, Monitor

    local i = 0
    repeat
      local p = gr.ao:nth_processor(i)
      if not p:isnil() and p:display_to_user() then
        if not p:to_plugininsert():isnil() and p:name() ~= "EQ" and p:name() ~= "Comp" then
          return true
        end
        if not p:to_send():isnil() then
          return true
        end
      end
      i = i + 1
    until p:isnil()

    return false
  end

  ---@param from Gall.Route
  ---@param to Gall.Route
  function moveProcessors(from, to)

    --[[
      hasMoveableProcessors() is used to check that {from} has anything to move; anything that will be picked up 
      by the "cut-processors" action.
      Otherwise, nothing will be picked up which means that the clipboard will still contain those picked up
      from a previous call, and those will be pasted into {to} instead, which is wrong.
    --]]

    if hasMoveableProcessors(from) then
      trace("  >> select-none...")
      Editor:access_action("Mixer", "select-none")  -- don't know if it's necessary
      trace("  >> select...")
      from:select()      
      trace("  >> select-all-processors...")
      Editor:access_action("Mixer", "select-all-processors")
      trace("  >> cut-processors...")
      Editor:access_action("Mixer", "cut-processors")    
      trace("  >> select...")
      to:select()
      trace("  >> paste-processors...")
      Editor:access_action("Mixer", "paste-processors")
    end
  end

  ---@param gr Gall.Route
  ---@param settings table
  function reorderProcessors(gr, settings)

    --[[
      At this point, the processors will be in this order:
      - EQ, Comp and Fader at the top, in the order that they were on this MB before the swap.
      - The procs that were moved here from the other MB, in the order they were there.
      So all we need do is move EQ, Comp and Fader into the same positions they were
        on the moved-from MB. {settings} has that info in its "procPosXXX" fields.
    ]]

    local t = {}

    --populate t with all visible procs except EQ, Comp and Fader
    for p in gr:getProcessors():iter() do
      local name = p.ao:display_name()
      if p.ao:display_to_user() and name ~= "EQ" and name ~= "Comp" and name ~= "Fader" then
        table.insert(t, p.ao)
      end
    end

    --For the others, get the position as they were on the from-MB and insert into t.
    --Needs to be done in position order, to avoid table-inserting beyond bounds.
    local tecf = {}
    table.insert(tecf, {pos = settings["procPosEq"], proc = gr:getProcessorsForName("EQ"):front().ao})
    table.insert(tecf, {pos = settings["procPosComp"], proc = gr:getProcessorsForName("Comp"):front().ao})
    table.insert(tecf, {pos = settings["procPosFader"], proc = gr:getProcessorsForName("Fader", true):front().ao})
    table.sort(tecf, function(a,b) return a.pos < b.pos end)
    for _,ecf in ipairs(tecf) do
      table.insert(t, ecf.pos, ecf.proc)
    end

    --build a ProcessorList to define the order
    local neworder = ARDOUR.ProcessorList()
    for _,p in ipairs(t) do
      neworder:push_back(p)
    end

    --apply the order
    gr.ao:reorder_processors(neworder, nil)

  end

  ---Pin-Port mappings and plugin instance counts.
  ---@param gr Gall.Route
  ---@param settings table
  function applyPluginInsertCustomizations(gr, settings)

    for _,t in ipairs(settings["pluginInserts"]) do
      local p = getProcessorFromNameAndOccurrenceNumber(gr, t["thisProcName"], t["thisProcNumber"])
      local pi = p:pi()
      local instanceCount = t["instances"]
      gr.ao:customize_plugin_insert(pi, instanceCount, t["outPorts"], t["inPorts"])
      for i = 1, instanceCount do
        local cm = t["inMaps"][i]
        pi:set_input_map(i-1, cm)
        cm = t["outMaps"][i]
        pi:set_output_map(i-1, cm)
      end
    end

  end

  ---If a to-be-swapped MB is 9..12, and it is to be moved to 1..8, then returns true if any MB 1..8 
  ---is sending to it.
  ---@param gr Gall.Route @An MB being swapped
  ---@param newNumber integer
  ---@return boolean
  function isMb9to12AndSentToByMbs(gr, newNumber)

    if not gr:isMb9to12() then return false end
    if newNumber > 8 then return false end

    for mbs in gr:mbGetSendsToThis(true):iter() do
      if mbs:getSource():isMb1to8() then return true end
    end

    return false
  end

  ---@param gr1 Gall.Route
  ---@param gr2 Gall.Route
  function setAllMbSends(gr1, gr2)

    local n1 = gr1:mbGetNumber()
    local n2 = gr2:mbGetNumber()

    --get the current active sends
    local oldSends1 = gr1:mbGetSendsToThis(true)
    local oldSends2 = gr2:mbGetSendsToThis(true)

    local is32c = Gall.App:is32c()

    local t1 = {}
    for mbs in oldSends1:iter() do
      local r = mbs:getSource().ao
      local i = mbs:getIndex()
      local t = {
        ["route"] = r:name(),
        ["level"] = r:send_level_controllable(i):get_value()
      }
      if is32c then
        t["panEnable"] = r:send_pan_azimuth_enable_controllable(i):get_value()
        t["pan"] = r:send_pan_azimuth_controllable(i):get_value()
      end
      table.insert(t1, t)
    end

    local t2 = {}
    for mbs in oldSends2:iter() do
      local r = mbs:getSource().ao
      local i = mbs:getIndex()
      local t = {
        ["route"] = r:name(),
        ["level"] = r:send_level_controllable(i):get_value()
      }
      if is32c then
        t["panEnable"] = r:send_pan_azimuth_enable_controllable(i):get_value()
        t["pan"] = r:send_pan_azimuth_controllable(i):get_value()
      end
      table.insert(t2, t)
    end

    --deactivate all
    for mbs in oldSends1:iter() do
      mbs:reset()
    end
    for mbs in oldSends2:iter() do
      mbs:reset()
    end

    --apply
    for _,t in ipairs(t1) do
      local r = Gall.Routes:get(t["route"])
      --get send to the other MB
      local mbs = Gall.MbSends:get(r, n2)
      local i = mbs:getIndex()
      if not r:isMb() or n2 >= 9 then
        r.ao:send_level_controllable(i):set_value(t["level"], PBD.GroupControlDisposition.NoGroup)
        if is32c then
          r.ao:send_pan_azimuth_enable_controllable(i):set_value(t["panEnable"], PBD.GroupControlDisposition.NoGroup)
          r.ao:send_pan_azimuth_controllable(i):set_value(t["pan"], PBD.GroupControlDisposition.NoGroup)
        end
        mbs:setActive(true)
      end
    end
    for _,t in ipairs(t2) do
      local r = Gall.Routes:get(t["route"])
      --get send to the other MB
      local mbs = Gall.MbSends:get(r, n1)
      local i = mbs:getIndex()
      if not r:isMb() or n1 >= 9 then
        r.ao:send_level_controllable(i):set_value(t["level"], PBD.GroupControlDisposition.NoGroup)
        if is32c then
          r.ao:send_pan_azimuth_enable_controllable(i):set_value(t["panEnable"], PBD.GroupControlDisposition.NoGroup)
          r.ao:send_pan_azimuth_controllable(i):set_value(t["pan"], PBD.GroupControlDisposition.NoGroup)
        end
        mbs:setActive(true)
      end
    end

  end

  ---@param gr1 Gall.Route
  ---@param gr2 Gall.Route
  ---@param settings1 table
  ---@param settings2 table
  function swapFoldbackSends(gr1, gr2, settings1, settings2)

    local t1 = settings1["foldbackSends"]
    local t2 = settings2["foldbackSends"]

    --remove all FB sends from both
    for _,t in ipairs(t1) do
      gr1:removeProcessor(t["targetName"])
    end
    for _,t in ipairs(t2) do
      gr2:removeProcessor(t["targetName"])
    end

    --build all 2's sends in 1, and all 1's sends in 2
    for _,t in ipairs(t2) do
      local targetFb = Gall.Routes:get(t["targetName"])
      gr1.ao:add_foldback_send(targetFb.ao, t["isPostFade"])
      gr1:getInternalSendTo(targetFb):setGain(t["gain"])
    end
    for _,t in ipairs(t1) do
      local targetFb = Gall.Routes:get(t["targetName"])
      gr2.ao:add_foldback_send(targetFb.ao, t["isPostFade"])
      local fbs = gr2:getInternalSendTo(targetFb)
      fbs:setGain(t["gain"])
      fbs:setActive(t["active"])
    end

  end

  ---@param gr Gall.Route
  ---@param settings table
  function fixSidechains(gr, settings)

    --[[
    If a processor on a MB has SCs then they are lost after the processor move, i.e. the processor
    has no SCs after being pasted into the other MB. This includes SCs via SC sends and direct connections.
     - The SC sends are also lost, on the other routes, unless they are also connected to other things
     (e.g. other non-lost SCs).
    
    If a MB is a SC source (has a send processor) then that send is moved like other processors.
    So, all good BUT:
    
      1) The send name is still the pre-move MB name. This name is seen in the target processor > 
      pin connections > SC source dropdown's display.
    
      2) SB sources are not always via SC sends: using the routing grid, a MB's outputs could be set
      as a processor's SC source. There is no processor for this on the MB - it is not a send, it is a 
      direct connection. If this MB's out is a SC source and it is swapped, then those connections need 
      to be remapped so that the swapped-MB is the source instead.
        - This is not handled in this function. It is handled by fixOutputConnections().

    There are other ways a MB could be involved in a SC. E.g. A MB's foldback send could be connected to
    a SC input via routing. I have no intention of tackling such possibilities. Therefore, this function
    is limited to:
      1) SC sends
      2) external sends (mainly because I can't find a way to distinguish those from SC sends)
      3) direct connections of route outputs to SCs on this MB

    --]]

    --all sidechain sends which were feeding processors on this MB before the swap (and have since been lost)
    for _,t in ipairs(settings["sidechainSendsTo"]) do      
      local p = getProcessorFromNameAndOccurrenceNumber(gr, t["thisProcName"], t["thisProcNumber"])
      local r = Gall.Routes:get(t["routeName"])
      local scs, ex = p:createSidechainSend(r, t["isPostFade"])
      if not scs then
        trace("*** FAILED TO CREATE SC SEND: "..t["routeName"].." > "..gr.ao:name()..": "..t["thisProcName"].." ("..t["thisProcNumber"]..")".." >> "..ex)
      else
        scs.ao:to_send():gain_control():set_value(t["gain"], PBD.GroupControlDisposition.NoGroup)
        if not t["active"] then scs.ao:deactivate() end
      end
     end

    --All direct connections which were feeding processors on this MB before the swap (and have since been lost).
    --Rather than recreate direct connections (no method yet exists for that), create post-fade sends.
    for _,t in ipairs(settings["sidechainRoutesTo"]) do      
      local p = getProcessorFromNameAndOccurrenceNumber(gr, t["thisProcName"], t["thisProcNumber"])
      local r = Gall.Routes:get(t["routeName"])
      local scs, ex = p:createSidechainSend(r, true)
      if not scs then
        trace("*** FAILED TO CREATE SC SEND: "..t["routeName"].." > "..gr.ao:name()..": "..t["thisProcName"].." ("..t["thisProcNumber"]..")".." >> "..ex)
      else
        scs.ao:to_send():gain_control():set_value(1.0, PBD.GroupControlDisposition.NoGroup)
      end
    end
  end

  ---Direct connections of this MB's output ports.
  ---@param gr Gall.Route
  ---@param settings table
  function fixOutputConnections(gr, settings)

    gr:disconnect()

    local t = settings["outputConnections"]
    local i = 0
    for op in gr:getPorts("out", "audio"):iter() do
      i = i + 1
      if t[i] then
        for _, cp in ipairs(t[i]) do
          op.ao:connect(cp)
        end
      end
    end

  end

  ---@param gr1 Gall.Route
  ---@param gr2 Gall.Route
  function swapAutomation(gr1, gr2, settings1, settings2)

    for k, autoType in pairs(Auto) do

      local ac1 = getAutomationControl(autoType, gr1)
      local ac2 = getAutomationControl(autoType, gr2)

      if not ac1 or not ac2 then
        --[[
          The Send controls are nil in Mixbus, and MB 9-12 in 32C.

          If we get to here then either:
            1) Neither MB has this control -> no automation -> do nothing.
            2) MB X has the control and MB Y does not. X's automation must be discarded because there's nowhere
               to copy it to, and Y doesn't have any automation to copy.
          Therefore, if the MB has this control then just discard its automation.
        ]]
          
        if ac1 then
          ac1:alist():clear_list()
        end
        if ac2 then
          ac2:alist():clear_list()
        end

      else

        local al1 = ac1:alist()
        local al2 = ac2:alist()
        local t = nil
        if al1:size() > 0 then
          --grab a temp copy of the gr1 events for this control
          t = {}
          for e in al1:events():iter() do
            table.insert(t, {when = e.when, value = e.value})
          end
          --clear the gr1 events for this control
          al1:clear_list()
        end     
        --copy the events from gr2 to gr1 for this control
        for e in al2:events():iter() do
          al1:add(e.when, e.value, false, true)
        end
        --clear the gr2 events for this control
        al2:clear_list()
        --copy the events from the temp copy to gr2 for this control
        if t then
          for _, e in ipairs(t) do
            al2:add(e.when, e.value, false, true)
          end
        end
      end
    end
  end

  function setAutomationStates(gr, settings)

    for _,t in ipairs(settings["autoStates"]) do
      local ac = getAutomationControl(t["autoType"], gr)
      if ac then
        ac:set_automation_state(t["state"])
        ac:alist():set_interpolation(t["interpolation"])
      end
    end
  end

  ---@param n1 integer
  ---@param n2 integer
  ---@param createSidechainSends boolean
  function swap(n1, n2, createSidechainSends)

    local gr1 = Gall.Routes:getMixbusForNumber(n1)
    local gr2 = Gall.Routes:getMixbusForNumber(n2)

    local name1 = gr1.ao:name()
    local name2 = gr2.ao:name()

    gr1.ao:set_name("-temp-")

    trace("getMixbusSettings 1...")
    local t1 = getMixbusSettings(gr1)
    trace("getMixbusSettings 2...")
    local t2 = getMixbusSettings(gr2)

    --Need this! The moveProcessors() calls below can crash the app without it. The "output connections" 
    --part of getMixbusSettings() above seems to be the cause - don't know why.
    collectgarbage()
    
    trace("setMixbusSettings 1...")
    setMixbusSettings(gr2, t1, name1, n1)
    trace("setMixbusSettings 2...")
    setMixbusSettings(gr1, t2, name2, n2)

    if isUsingPtb then
      trace("moveProcessors 1...")
      moveProcessors(gr2, getPtb())
      trace("moveProcessors 2...")
      moveProcessors(gr1, gr2)
      trace("moveProcessors 3...")
      moveProcessors(getPtb(), gr1)
    end
    
    trace("reorderProcessors 1...")
    reorderProcessors(gr1, t2)
    trace("reorderProcessors 2...")
    reorderProcessors(gr2, t1)

    trace("applyPluginInsertCustomizations 1...")
    applyPluginInsertCustomizations(gr1, t2)
    trace("applyPluginInsertCustomizations 2...")
    applyPluginInsertCustomizations(gr2, t1)

    trace("swapFoldbackSends...")
    swapFoldbackSends(gr1, gr2, t1, t2)

    if createSidechainSends then
      trace("fixSidechains 1...")
      fixSidechains(gr1, t2)
      trace("fixSidechains 2...")
      fixSidechains(gr2, t1)
    end

    trace("fixOutputConnections 1...")
    fixOutputConnections(gr1, t2)
    trace("fixOutputConnections 2...")
    fixOutputConnections(gr2, t1)

    trace("setAllMbSends...")
    setAllMbSends(gr1, gr2)

    trace("swapAutomation ...")
    swapAutomation(gr1, gr2)

    trace("setAutomationStates 1 ...")
    setAutomationStates(gr1, t2)
    trace("setAutomationStates 2 ...")
    setAutomationStates(gr2, t1)

  end

  function showDialog()

    local tAllMbs = {}
    local tVisibleMbs = {}

    for gr in Gall.Routes:getMixbusAll():iter() do
      table.insert(tAllMbs, { number = gr:mbGetNumber(), name = gr.ao:name(), hidden = gr.ao:is_hidden() })
    end

    --only show unhidden MBs as options - select() code needs them to be unhidden
    for _,v in ipairs(tAllMbs) do
      if not v.hidden then
        table.insert(tVisibleMbs, v)
      end
    end

    local optMbOptions = {}
    for _,v in ipairs(tVisibleMbs) do
      optMbOptions[v.name] = v.name
    end

    local dialog_options = {}

    local info = 
      "*** SAVE FIRST! *** This could seriously mess up your session if something goes wrong."..
      "\n\nThis effectively 'moves' Mixbusses by moving everything about them to different positions. E.g. by swapping MB 1 and MB 2 all of MB 1's settings,"..
      "\nprocessors and automation will be copied to MB 2, and vice versa. Everything that was sending to MB 1 will now send to MB 2 instead, and vice versa."..
       "\n\nAny number of swaps can be done at once. Use the controls below to say what the new order should be."..
      "\n\nThere might be long pauses where nothing seems to be happening. Wait... a message will say when it's finished."..
      "\n\nThose with a name of \"Mixbus {number}\" will be renamed after the move. E.g. \"Mixbus 5\" moved to be the 3rd Mixbus will be renamed \"Mixbus 3\"."..
      "\n\nIt cannot move Mixbusses that have any Inserts - it will tell you if you are trying to move any that do. Also, foldback panning will be lost."

    info = info.."\n"

    table.insert(dialog_options, {
      type = "label",
      title = info,
      align = "left",
      col = 0,
      colspan = 8
    })

    for n = 1, #tAllMbs / 2 do
      local i = n
      local v = tAllMbs[i]
      table.insert(dialog_options, { type = "label", title = i..":", align = "right", col = 0, colspan = 1 })
      if v.hidden then
        table.insert(dialog_options, { type = "label", title = "(hidden - cannot be moved)", align = "left", col = 1, colspan = 1 })
      else
        table.insert(dialog_options, {
          type = "dropdown",
          key = "MB"..i,
          title = "",
          values = optMbOptions,
          default = v.name,
          align = "left",
          col = 1,
          colspan = 1
        })
      end
      i = math.tointeger(n + (#tAllMbs / 2))
      v = tAllMbs[i]
      table.insert(dialog_options, { type = "label", title = i..":", align = "right", col = 2, colspan = 1 })
      if v.hidden then
        table.insert(dialog_options, { type = "label", title = "(hidden - cannot be moved)", align = "left", col = 3, colspan = 1 })
      else
        table.insert(dialog_options, {
          type = "dropdown",
          key = "MB"..i,
          title = "",
          values = optMbOptions,
          default = v.name,
          align = "left",
          col = 3,
          colspan = 1
        })
      end
    end

    --to shift the dropdowns section to the left a bit
    table.insert(dialog_options, { type = "label", title = "", align = "left", col = 4, colspan = 1 })

    local sidechainInfo = 
      "\nIf a moved Mixbus has any plugins with sidechains in use then the sends to those sidechains are lost. Ticking the option below will attempt to"..
      "\nrecreate those sends. HOWEVER, this might result in strange (or wrong) names for those sends and/or the names of the sources in the plugin's Pin"..
      "\nConnections. Or a send might fail to be created altogether. Plus it doesn't support send panning."..
      "\nSo you might prefer to skip this part and manually recreate the sidechain connections afterwards."

    table.insert(dialog_options, {
      type = "label",
      title = sidechainInfo,
      align = "left",
      col = 0,
      colspan = 8
    })

    table.insert(dialog_options, {
      type = "checkbox",
      key = "sidechains",
      title = "Recreate sidechain sends",
      default = true,
      col = 0,
      colspan = 8
    })

    local od = LuaDialog.Dialog ("Move Mixbusses", dialog_options)

    local tResult = {}
    local tSwaps = nil
    local tAllSwappableMbs = nil
    local cancelled = false

    repeat
      local rv = od:run()
      cancelled = rv == nil

      local canProceed = true

      if not cancelled then
        tResult.sidechains = rv.sidechains
        tSwaps = {}
        tAllSwappableMbs = {}

        for _,v in ipairs(tVisibleMbs) do
          local currName = v.name
          local newName = rv["MB"..v.number]
          table.insert(tAllSwappableMbs, { mbNum = v.number, toBe = newName })
          if currName ~= newName then
            table.insert(tSwaps, { mbNum = v.number, toBe = newName })
          end
        end

        --any changes?
        if canProceed then
          if #tSwaps == 0 then
            Gall.Dlg:cannotProceed("No changes selected")
            canProceed = false
          end
        end

        --any duplicates?
        if canProceed then
          for _,v in ipairs(tVisibleMbs) do
            local count = 0
            for _,v2 in ipairs(tAllSwappableMbs) do
              if v2.toBe == v.name then
                count = count + 1
              end
            end
            if count > 1 then
              Gall.Dlg:cannotProceed("\""..v.name.."\" is selected more than once")
              canProceed = false
              break
            end
          end
        end

        --any Inserts?
        if canProceed then
          for _,v in ipairs(tSwaps) do
            if Gall.Routes:get(v.toBe):getInserts():size() > 0 then
              Gall.Dlg:cannotProceed("Insert(s) found on one or more of the to-be-moved Mixbusses."..
              "\n\nNOTE: this is referring to \"Insert\" processors, not \"Plugin Insert\" processors."..
              "\n\nThis script cannot move Inserts and it can be broken by their presence."..
              "\n\nPlease remove the Inserts and recreate them after this script has finished running, or don't move any Mixbusses with Inserts.")
              canProceed = false
              break
            end
          end
        end

        --any MB Sends lost?
        if canProceed then
          local affectedMBs = nil
          for _,v in ipairs(tSwaps) do
            if isMb9to12AndSentToByMbs(Gall.Routes:get(v.toBe), v.mbNum) then
              if affectedMBs then
                affectedMBs = affectedMBs..", "..v.toBe
              else
                affectedMBs = v.toBe
              end
            end
          end
          if affectedMBs then
            local msg = "The following are currently in the Mixbus 9..12 range, are being moved to the 1..8 range, "..
              "and other Mixbusses are sending to them:"..
              "\n\n  "..affectedMBs..
              "\n\nAs they will be moved to the 1..8 range they will no longer be able to be sent to by other Mixbusses. "..
              "The sends will be deactivated."..
              "\n\nThis is an indication that these moves should not be done, as those sends are probably there for a reason."..
              "\n\nAre you sure you wish to move those Mixbusses?"

              canProceed = Gall.Dlg:prompt("Discard Mixbus->Mixbus sends?", msg, true)
          end
        end

      end

    until canProceed or cancelled

    od = nil
    collectgarbage()

    if cancelled then
      return nil
    end

    tResult.swaps = tSwaps

    return tResult
  end

  function fixMixbusNames()

    --fix "Mixbus X" names
    -- e.g. if is "Mixbus 3" but has been swapped with MB 5 then rename to "Mixbus 5"

    local toFix = Gall.List.Route:new()

    for r in Gall.Routes:getMixbusAll():iter() do
      local name = r.ao:name()
      local preName = string.sub(name, 1, 7)
      if string.upper(preName) == "MIXBUS " then
        if tonumber(string.sub(name, 8)) then
          toFix:add(r)
        end
      end
    end

    for r in toFix:iter() do
      r.ao:set_name("XXX")
    end

    for r in toFix:iter() do
      r.ao:set_name("Mixbus "..r:mbGetNumber())
    end

  end

  function main()

    if Gall.App:isArdour() then
      Gall.Dlg:cannotProceed("This only works with Mixbus and Mixbus32C.")
      return
    end

    --note: Mixbus section does NOT need to be visible for plugin swap to work. Likewise, being in Editor view works too!

    --todo: insert
    --[[
        Is not moved via processor swap, as they can't be copied/cut/pasted even manually.

        Seems there's no way to create inserts in Lua either. So if a to-be-swapped MB contains any
        inserts then this entire operation must be aborted. It will cause problems with the proc reordering
        step, possibly table bounds errors therefore script crash.
         - Yes there is! Editor:access_action("ProcessorMenu", "newinsert")
           - But how do we find the newly created one? Could get the IDs of all processors on the route first
             then create the insert, then rescan to find the ID that wasn't there before.
           - Also, this seems to put in on the track to the right of the selected track. MBs seem OK though.
-   -]]

    --[[
      The swapping logic...

      E.g. for this new ordering (NEW column)

      MB#  CURRENT NEW
       1   Drums   Bass
       2   Bass    Vox
       3   Gtr     Gtr
       4   Vox     Drums

      Get all changes except the last*. In this example, only those for MB# 1 & 2, as 3 is not a change and 4 is the last change.
      There will be one swap for each of those changes. The two MBs to swap are:
      1) the MB# of that change
      2) the number NOW** of the MB whose name matches the NEW of that change
      * The last swap isn't required because it is done as a by-product of the other swap(s). In this example, Drums is moved
        to position 2 by the 1st swap and position 4 by the 2nd.
      ** NOW meaning at the time before this particular swap, which is not necessarily where that MB was before any swapping began.

      After swap 1 (MB# = 1, Bass is currently 2, so swap MBs 1 & 2):
      MB#  NOW
       1   Bass
       2   Drums
       3   Gtr
       4   Vox

      After swap 2 (MB# = 2, Vox is currently 4, so swap MBs 2 & 4):
      MB#  NOW
       1   Bass
       2   Vox
       3   Gtr
       4   Drums

      This now matches the NEW ordering.
    ]]

    local tResult = showDialog()
    if not tResult then return end
    
    --create PTB, only needed if any moveable processors on any MBs being swapped
    for _,t in ipairs(tResult.swaps) do
      local gr = Gall.Routes:getMixbusForName(t.toBe)
      if hasMoveableProcessors(gr) then
        if not createPluginTransferBus() then return end
        isUsingPtb = true
        break
      end
    end

    --remove the last swap
    table.remove(tResult.swaps, #tResult.swaps)

    for _,t in ipairs(tResult.swaps) do
      local n1 = t.mbNum
      local n2 = Gall.Routes:getMixbusForName(t.toBe):mbGetNumber()
      swap(n1, n2, tResult.sidechains)
    end

    fixMixbusNames()

    local msg = "The operation is complete"
    if isUsingPtb then
      msg = msg..".\n\nAfter closing this message you will see a prompt to remove a bus. This was a temporary "..
            "bus created for processor-copying reasons. It should be removed."
    end
    Gall.Dlg:msg("Done", msg)

    if isUsingPtb then
      trace("removing temp track: selecting...")
      local selected = getPtb():select()
      if selected then
        trace("removing temp track: removing...")
        Editor:access_action("Editor", "remove-track")
      else
        Gall.Dlg:msg("Delete temp bus", "The temporary bus \""..PTB_NAME.."\" could not be removed. Please remove it.")
      end
    end

  end

  main()
  collectgarbage()

end end

function icon (params) return function (ctx, width, height, fg)

  local iconText = "MMB"
  local iconFont = "Liberation Sans Narrow"-- "ArdourMono"
  local iconFontScale = 0.5

  local txt = Cairo.PangoLayout (ctx, iconFont.." ".. math.ceil(math.min(width, height) * iconFontScale) .. "px")
  txt:set_text (iconText)
  local tw, th = txt:get_pixel_size ()
  ctx:move_to (.5 * (width - tw), .5 * (height - th))
  ctx:set_source_rgba(ARDOUR.LuaAPI.color_to_rgba (0x80afffff))
  txt:show_in_cairo_context (ctx)
end end
