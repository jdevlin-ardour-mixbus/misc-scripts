ardour {
  ["type"] = "EditorAction",
  name = "[JD] Create Strip Name Plugins v1.0",
  license     = "MIT",
  author      = "John Devlin",
  description = [[Creates a JD Strip Name plugin on every track and bus that doesn't already have one.]]
}

function factory (params) return function ()

  local pluginName = "JD Strip Name"

  ---@param r ARDOUR.Route
  ---@return boolean
  function pluginExists(r)
    local i = 0
    repeat
      local p = r:nth_processor(i)
      if not p:isnil() and p:name() == pluginName then
        return true
      end
      i = i + 1
    until p:isnil()
    return false
  end

  ---@param r ARDOUR.Route
  ---@return boolean @true if r is a Mixbus
  function isMixbus(r)
    local mbFlag = 4096
    return r:presentation_info_ptr():flags() & mbFlag == mbFlag
  end

  ---@param r ARDOUR.Route
  ---@return boolean @true if r is a track or bus and not a Mixbus
  function isValidTarget(r)
    if r:presentation_info_ptr():flags() & ARDOUR.PresentationInfo.Flag.AudioBus == ARDOUR.PresentationInfo.Flag.AudioBus
      or r:presentation_info_ptr():flags() & ARDOUR.PresentationInfo.Flag.AudioTrack == ARDOUR.PresentationInfo.Flag.AudioTrack
      or r:presentation_info_ptr():flags() & ARDOUR.PresentationInfo.Flag.MidiBus == ARDOUR.PresentationInfo.Flag.MidiBus
      or r:presentation_info_ptr():flags() & ARDOUR.PresentationInfo.Flag.MidiTrack == ARDOUR.PresentationInfo.Flag.MidiTrack then
      return not isMixbus(r)
    end
    return false
  end

  local targets = {}

  for r in Session:get_routes():iter() do
    if isValidTarget(r) and not pluginExists(r) then
      table.insert(targets, r)
    end
  end

  if #targets == 0 then
    local msg = "Either no tracks/busses exist or they all already have a "..pluginName.." plugin."
    local md = LuaDialog.Message("Nothing to do", msg, LuaDialog.MessageType.Info, LuaDialog.ButtonType.Close)
    md:run()
    md = nil
  else
    local msg = "This creates a "..pluginName.." plugin on every track and bus that doesn't already have one: "..
      #targets.." strip(s).\n\nContinue?"
    local md = LuaDialog.Message("Create plugins?", msg, LuaDialog.MessageType.Question, LuaDialog.ButtonType.Yes_No)
    local rv = md:run()
    md = nil
    if rv == LuaDialog.Response.Yes then
      for _,r in ipairs(targets) do
        local proc = ARDOUR.LuaAPI.new_plugin(Session, pluginName, ARDOUR.PluginType.Lua, "")
        r:add_processor_by_index(proc, 0, nil, true)
      end
    end
  end

  collectgarbage ()

end end

function icon (params) return function (ctx, width, height, fg)

  local iconText = "CSNP"
  local iconFont = "ArdourMono"
  local iconFontScale = 0.4

  local txt = Cairo.PangoLayout (ctx, iconFont.." ".. math.ceil(math.min(width, height) * iconFontScale) .. "px")
  txt:set_text (iconText)
  local tw, th = txt:get_pixel_size ()
  ctx:move_to (.5 * (width - tw), .5 * (height - th))
  ctx:set_source_rgba(ARDOUR.LuaAPI.color_to_rgba (0x80afffff))
  txt:show_in_cairo_context (ctx)
end end
